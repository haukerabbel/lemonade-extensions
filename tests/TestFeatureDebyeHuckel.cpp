/*****************************************************************************/
/**
 * @file
 * @brief Tests for the class FeatureDebyeHuckel
 * 
 * @author Martin
 * @date 07.07.2014
 * */
/*****************************************************************************/


#include "gtest/gtest.h"
#include <iostream>
#include <sstream>
#include <LeMonADE/core/Molecules.h>
#include <LeMonADE/core/Ingredients.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/io/FileImport.h>
#include <LeMonADE/feature/FeatureBondset.h>
#include <LeMonADE/updater/moves/MoveBase.h>
#include <LeMonADE/updater/moves/MoveLocalSc.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFile.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>

#include <LeMonADE-Extensions/features/FeatureDebyeHuckel.h>
using namespace std;

// class with body that is needed for every test, only used by TEST_F()
/*****************************************************************************/
/**
 * @class TestFeatureDebyeHuckel
 * @brief checking different classes and functions depending to FeatureDebyeHuckel
 * */
/*****************************************************************************/
class TestFeatureDebyeHuckel: public ::testing::Test{
protected:
  typedef LOKI_TYPELIST_2(FeatureMoleculesIO,FeatureDebyeHuckel) Features;
  typedef ConfigureSystem<VectorInt3,Features> Config;
  typedef Ingredients<Config> MyIngredients;
  
  /* suppress cout output for better readability -->un-/comment here:*/    
public:
  //redirect cout output
  virtual void SetUp(){
    originalBuffer=cout.rdbuf();
    cout.rdbuf(tempStream.rdbuf());
  };
  //restore original output
  virtual void TearDown(){
    cout.rdbuf(originalBuffer);
  };
private:
  std::streambuf* originalBuffer;
  std::ostringstream tempStream;
  /* ** */
};

/*****************************************************************************/
TEST_F(TestFeatureDebyeHuckel, ConfigurePotential)
{
	MyIngredients ing;
	
	
	EXPECT_EQ(ing.getBjerrumLength(),0.0);
	EXPECT_EQ(ing.getKappa(),0.0);
	EXPECT_EQ(ing.getCutoffRadius(),0.0);
	
	ing.setBoxX(64);
	ing.setBoxY(128);
	ing.setBoxZ(64);
	ing.setPeriodicX(1);
	ing.setPeriodicY(1);
	ing.setPeriodicZ(1);
	
	ing.synchronize(ing);
	EXPECT_NEAR(ing.getCutoffRadius(),std::sqrt(6144.0),0.000005);
	
	ing.setBjerrumLength(1.0);
	EXPECT_EQ(ing.getBjerrumLength(),1.0);
	
	ing.setKappa(0.2);
	EXPECT_EQ(ing.getKappa(),0.2);
	
	ing.setKappa(0.0);
	EXPECT_EQ(ing.getKappa(),0.0);
	ing.setBjerrumLength(0.0);
	EXPECT_EQ(ing.getBjerrumLength(),0.0);
	
	ing.setCutoffRadius(10.2);
	EXPECT_EQ(ing.getCutoffRadius(),10.2);
	
	
	
	
}


/*****************************************************************************/
TEST_F(TestFeatureDebyeHuckel, ConfigureAndSyncVerletList)
{
	MyIngredients ing;
	ing.setBoxX(64);
	ing.setBoxY(64);
	ing.setBoxZ(64);
	ing.setPeriodicX(1);
	ing.setPeriodicY(1);
	ing.setPeriodicZ(1);
	
	EXPECT_EQ(ing.getVerletSkin(),0.0);
	EXPECT_EQ(ing.getVerletListLength(),0);
	
	ing.setVerletSkin(1.0);
	EXPECT_EQ(ing.getVerletSkin(),1.0);
	
	ing.setVerletSkin(5.2);
	EXPECT_EQ(ing.getVerletSkin(),5.2);
	
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),0);
	
	ing.setUseVerletList(true);
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),0);
	EXPECT_NEAR(ing.getCutoffRadius(),std::sqrt(3072.0),0.000005);
	
	// now add particles, synchronize, and check length of verlet list 
	//for different configurations (charges, no charges, different skin, Rcut)
	ing.modifyMolecules().addMonomer(1,1,1);
	ing.modifyMolecules().addMonomer(11,1,1);
	ing.modifyMolecules().addMonomer(21,1,1);
	
	//no charges
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),0);
	
	//charges, but cutoff and skin small
	ing.modifyMolecules()[0].setCharge(1);
	ing.modifyMolecules()[1].setCharge(1);
	ing.modifyMolecules()[2].setCharge(1);
	
	ing.setVerletSkin(2.0);
	ing.setCutoffRadius(3.0);
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),0);
	
	//charges, cutoff and skin enough for neighbouring pairs
	ing.setVerletSkin(5.0);
	ing.setCutoffRadius(5.0);
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),4);
	
	//charges, cutoff and skin enough for neighbouring pairs
	//but middle charge deleted->empty list
	ing.modifyMolecules()[1].setCharge(0);
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),0);
	
	//charges, cutoff and skin enough for neighbouring pairs
	//this time a negative charge
	ing.modifyMolecules()[1].setCharge(-1);
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),4);
	
	//charges, cutoff and skin enough for all pairs
	ing.setVerletSkin(15.0);
	ing.setCutoffRadius(5.0);
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),6);
	
	//charges, cutoff and skin enough for all pairs
	ing.setVerletSkin(6.0);
	ing.setCutoffRadius(15.0);
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),6);
	
	ing.setUseVerletList(false);
	ing.synchronize(ing);
	EXPECT_EQ(ing.getVerletListLength(),0);
}
/*****************************************************************************/
TEST_F(TestFeatureDebyeHuckel, CheckReadWrite)
{
	MyIngredients ingredients;
	ingredients.setBoxX(64);
	ingredients.setBoxY(128);
	ingredients.setBoxZ(64);
	ingredients.setPeriodicX(1);
	ingredients.setPeriodicY(1);
	ingredients.setPeriodicZ(1);
	ingredients.modifyMolecules().addMonomer(1,1,1);
	ingredients.setBjerrumLength(4.0);
	ingredients.setKappa(2.0);
	ingredients.setCutoffRadius(10.2);
	ingredients.synchronize(ingredients);
	
	string filename("tmpTestFeatureDebyeHuckel.bfm");
	AnalyzerWriteBfmFile<MyIngredients> writeobject(filename, ingredients);
	writeobject.initialize();
	writeobject.execute();
	
	MyIngredients ingredients2;
	FileImport<MyIngredients> fileImport(filename,ingredients2);
	fileImport.initialize();
	EXPECT_EQ(ingredients2.getBjerrumLength(),4.0);
	EXPECT_EQ(ingredients2.getKappa(),2.0);
	EXPECT_EQ(ingredients2.getCutoffRadius(),10.2);
	remove(filename.c_str());
	
	
	MyIngredients ingredients3;
	ReadDebyeHuckel <MyIngredients> command(ingredients3);
	
	//check reaction to correct standard input
	istringstream inputStandard("1.0 0.1");
	command.setInputStream(&inputStandard);
	EXPECT_NO_THROW(command.execute());
	EXPECT_EQ(ingredients3.getBjerrumLength(),1.0);
	EXPECT_EQ(ingredients3.getKappa(),0.1);
	
	//check reaction to spacers
	istringstream inputSpacer("\n  2.0      0.2  ");
	command.setInputStream(&inputSpacer);
	EXPECT_NO_THROW(command.execute());
	EXPECT_EQ(ingredients3.getBjerrumLength(),2.0);
	EXPECT_EQ(ingredients3.getKappa(),0.2);
	
	//check reaction to empty input
	istringstream inputEmpty("\n ");
	command.setInputStream(&inputEmpty);
	EXPECT_THROW(command.execute(),std::runtime_error);
	
	//check reaction to wrong format
	istringstream inputWrong1("abc 1.0");
	command.setInputStream(&inputWrong1);
	EXPECT_THROW(command.execute(),std::runtime_error);
	
	//check reaction to wrong format
	istringstream inputWrong1b("1.0 abc");
	command.setInputStream(&inputWrong1b);
	EXPECT_THROW(command.execute(),std::runtime_error);
	
	//check reaction to wrong number of arguments
	istringstream inputWrong2(" 0.2");
	command.setInputStream(&inputWrong2);
	EXPECT_THROW(command.execute(),std::runtime_error);
	
	
  
}
/*****************************************************************************/

TEST_F(TestFeatureDebyeHuckel, CheckMoveOverlap)
{
  MyIngredients ingredients;
    ingredients.setBoxX(64);
  ingredients.setBoxY(128);
  ingredients.setBoxZ(64);
  ingredients.setPeriodicX(1);
  ingredients.setPeriodicY(1);
  ingredients.setPeriodicZ(1);
  ingredients.modifyMolecules().addMonomer(1,1,1);
  ingredients.modifyMolecules().addMonomer(2,1,1);
  ingredients.setBjerrumLength(4.0);
  ingredients.setKappa(0.2);
  ingredients.synchronize(ingredients);
  
  MoveBase move;
  EXPECT_TRUE(move.check(ingredients));
  
  MoveLocalSc localmove;
  while(localmove.getDir().getX()!=1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  
  //none of the monomers are charged and we dont use excluded volume
  EXPECT_TRUE(localmove.check(ingredients));
  localmove.resetProbability();
  
  ingredients.modifyMolecules()[0].setCharge(1);
  ingredients.modifyMolecules()[1].setCharge(-1);
  ingredients.synchronize(ingredients);
  //now the monomers are charged, so overlap should be forbidden
  EXPECT_FALSE(localmove.check(ingredients));
  
}

TEST_F(TestFeatureDebyeHuckel, CheckMoveRegular)
{
  MyIngredients ingredients;
  ingredients.setBoxX(64);
  ingredients.setBoxY(128);
  ingredients.setBoxZ(64);
  ingredients.setPeriodicX(1);
  ingredients.setPeriodicY(1);
  ingredients.setPeriodicZ(1);
  
  ingredients.modifyMolecules().addMonomer(1,1,1);
  ingredients.modifyMolecules().addMonomer(5,1,1);
  ingredients.modifyMolecules().addMonomer(1,5,1);
  ingredients.modifyMolecules().addMonomer(5,5,1);
  ingredients.modifyMolecules().addMonomer(1,1,5);
  
  ingredients.modifyMolecules()[0].setCharge(0);
  ingredients.modifyMolecules()[1].setCharge(1);
  ingredients.modifyMolecules()[2].setCharge(2);
  ingredients.modifyMolecules()[3].setCharge(-1);
  ingredients.modifyMolecules()[4].setCharge(-2);
  
  ingredients.setBjerrumLength(4.0);
  ingredients.setKappa(0.2);
  ingredients.synchronize(ingredients);
  
  
  MoveLocalSc localmove;
  
  //moving monomer 0 should be ok in any direction
  while(localmove.getDir().getX()!=1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getX()!=-1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=-1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  //moving monomer 1 should give the expected probabilities
  while(localmove.getDir().getX()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.97627,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getX()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.97627,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),1.14482,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.94106,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),1.13106,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.88844,0.000005);
  localmove.resetProbability();
}

TEST_F(TestFeatureDebyeHuckel, CheckMoveRegularVerlet)
{
  MyIngredients ingredients;
  ingredients.setBoxX(64);
  ingredients.setBoxY(128);
  ingredients.setBoxZ(64);
  ingredients.setPeriodicX(1);
  ingredients.setPeriodicY(1);
  ingredients.setPeriodicZ(1);
  
  ingredients.modifyMolecules().addMonomer(1,1,1);
  ingredients.modifyMolecules().addMonomer(5,1,1);
  ingredients.modifyMolecules().addMonomer(1,5,1);
  ingredients.modifyMolecules().addMonomer(5,5,1);
  ingredients.modifyMolecules().addMonomer(1,1,5);
  //this is a test particle within the verlet skin,
  //which should not have any influence on the potential
  //so that the values are the same as in the previous test
  ingredients.modifyMolecules().addMonomer(17,1,1);
  
  ingredients.modifyMolecules()[0].setCharge(0);
  ingredients.modifyMolecules()[1].setCharge(1);
  ingredients.modifyMolecules()[2].setCharge(2);
  ingredients.modifyMolecules()[3].setCharge(-1);
  ingredients.modifyMolecules()[4].setCharge(-2);
  ingredients.modifyMolecules()[5].setCharge(4);
  
  ingredients.setBjerrumLength(4.0);
  ingredients.setKappa(0.2);
  ingredients.setCutoffRadius(10.0);
  ingredients.setVerletSkin(5.0);
  ingredients.setUseVerletList(true);
  
  ingredients.synchronize(ingredients);
  
  
  MoveLocalSc localmove;
  
  //moving monomer 0 should be ok in any direction
  while(localmove.getDir().getX()!=1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getX()!=-1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=-1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=0)
	  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));
  EXPECT_EQ(localmove.getProbability(),1.0);
  localmove.resetProbability();
  
  //moving monomer 1 should give the expected probabilities
  while(localmove.getDir().getX()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.97627,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getX()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.97627,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),1.14482,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.94106,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),1.13106,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.88844,0.000005);
  localmove.resetProbability();
  
  //same checks again but this time without verlet list, but still using the
  //same cutoff radius
  //moving monomer 1 should give the expected probabilities
  ingredients.setUseVerletList(false);
  ingredients.synchronize(ingredients);
  
  while(localmove.getDir().getX()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.97627,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getX()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.97627,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),1.14482,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.94106,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),1.13106,0.000005);
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  EXPECT_NEAR(localmove.getProbability(),0.88844,0.000005);
  localmove.resetProbability();
}

TEST_F(TestFeatureDebyeHuckel, CheckMoveMinimumImage)
{
  MyIngredients ingredients;
  ingredients.setBoxX(8);
  ingredients.setBoxY(16);
  ingredients.setBoxZ(32);
  ingredients.setPeriodicX(1);
  ingredients.setPeriodicY(1);
  ingredients.setPeriodicZ(1);
  
  ingredients.modifyMolecules().addMonomer(0,0,0);
  ingredients.modifyMolecules().addMonomer(3,0,0);
  ingredients.modifyMolecules().addMonomer(5,0,0);
  ingredients.modifyMolecules().addMonomer(0,3,0);
  ingredients.modifyMolecules().addMonomer(0,13,0);
  ingredients.modifyMolecules().addMonomer(0,0,3);
  ingredients.modifyMolecules().addMonomer(0,0,29);
  
  ingredients.modifyMolecules()[0].setCharge(1);
  ingredients.modifyMolecules()[1].setCharge(-1);
  ingredients.modifyMolecules()[2].setCharge(0);
  
  ingredients.setBjerrumLength(4.0);
  ingredients.setKappa(0.2);
  ingredients.synchronize(ingredients);
  
  
  MoveLocalSc localmove;
  
  
  //calculate probabilities of moving monomer 1 along x-axis
  while(localmove.getDir().getX()!=1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono1PosX=localmove.getProbability();
  localmove.resetProbability();
  
  while(localmove.getDir().getX()!=-1 ||  localmove.getIndex()!=1)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono1NegX=localmove.getProbability();
  localmove.resetProbability();
  
  ingredients.modifyMolecules()[1].setCharge(0);
  ingredients.modifyMolecules()[2].setCharge(-1);
  ingredients.synchronize(ingredients);
  
  //moving monomer 2 in the opposite direction as 1 should now give the same probabilities.
  while(localmove.getDir().getX()!=1 ||  localmove.getIndex()!=2)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono2PosX=localmove.getProbability();
  localmove.resetProbability();
  
  while(localmove.getDir().getX()!=-1 ||  localmove.getIndex()!=2)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono2NegX=localmove.getProbability();
  localmove.resetProbability();
  
  
  EXPECT_DOUBLE_EQ(probMono1NegX,probMono2PosX);
  EXPECT_DOUBLE_EQ(probMono2NegX,probMono1PosX);
  
  
  //calculate probabilities of moving monomer 3 along y-axis
  ingredients.modifyMolecules()[3].setCharge(-1);
  ingredients.modifyMolecules()[4].setCharge(0);
  ingredients.synchronize(ingredients);
  
  while(localmove.getDir().getY()!=1 ||  localmove.getIndex()!=3)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono3PosY=localmove.getProbability();
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=-1 ||  localmove.getIndex()!=3)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono3NegY=localmove.getProbability();
  localmove.resetProbability();
  
  ingredients.modifyMolecules()[3].setCharge(0);
  ingredients.modifyMolecules()[4].setCharge(-1);
  ingredients.synchronize(ingredients);
  
  //moving monomer 2 in the opposite direction as 4 should now give the same probabilities.
  while(localmove.getDir().getY()!=1 ||  localmove.getIndex()!=4)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono4PosY=localmove.getProbability();
  localmove.resetProbability();
  
  while(localmove.getDir().getY()!=-1 ||  localmove.getIndex()!=4)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono4NegY=localmove.getProbability();
  localmove.resetProbability();
  
  
  EXPECT_DOUBLE_EQ(probMono4NegY,probMono3PosY);
  EXPECT_DOUBLE_EQ(probMono3NegY,probMono4PosY);
  
  //calculate probabilities of moving monomer 1 along y-axis
  ingredients.modifyMolecules()[5].setCharge(-1);
  ingredients.modifyMolecules()[6].setCharge(0);
  ingredients.synchronize(ingredients);
  
  while(localmove.getDir().getZ()!=1 ||  localmove.getIndex()!=5)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono5PosZ=localmove.getProbability();
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=5)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono5NegZ=localmove.getProbability();
  localmove.resetProbability();
  
  ingredients.modifyMolecules()[5].setCharge(0);
  ingredients.modifyMolecules()[6].setCharge(-1);
  ingredients.synchronize(ingredients);
  
  //moving monomer 2 in the opposite direction as 1 should now give the same probabilities.
  while(localmove.getDir().getZ()!=1 ||  localmove.getIndex()!=6)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono6PosZ=localmove.getProbability();
  localmove.resetProbability();
  
  while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=6)
	  localmove.init(ingredients);
  localmove.check(ingredients);
  double probMono6NegZ=localmove.getProbability();
  localmove.resetProbability();
  
  
  EXPECT_DOUBLE_EQ(probMono6NegZ,probMono5PosZ);
  EXPECT_DOUBLE_EQ(probMono5NegZ,probMono6PosZ);
  
}
/*****************************************************************************/

TEST_F(TestFeatureDebyeHuckel, Synchronize)
{
  MyIngredients ingredients;
    ingredients.setBoxX(64);
  ingredients.setBoxY(128);
  ingredients.setBoxZ(64);
  ingredients.setPeriodicX(1);
  ingredients.setPeriodicY(1);
  ingredients.setPeriodicZ(1);
  
  ingredients.modifyMolecules().addMonomer(1,1,1);
  ingredients.synchronize(ingredients);
  EXPECT_EQ(0,ingredients.getChargedMonomers().size());
  
  ingredients.modifyMolecules()[0].setCharge(1);
  ingredients.synchronize(ingredients);
  EXPECT_EQ(1,ingredients.getChargedMonomers().size());
  
  ingredients.modifyMolecules().addMonomer(10,1,1);
  ingredients.modifyMolecules()[1].setCharge(-1);
  ingredients.synchronize(ingredients);
  EXPECT_EQ(2,ingredients.getChargedMonomers().size());
  
}

TEST_F(TestFeatureDebyeHuckel, CalculatePotential)
{
	MyIngredients ingredients;
	ingredients.setBoxX(64);
	ingredients.setBoxY(128);
	ingredients.setBoxZ(64);
	ingredients.setPeriodicX(1);
	ingredients.setPeriodicY(1);
	ingredients.setPeriodicZ(1);
	
	ingredients.modifyMolecules().addMonomer(1,1,1);
	ingredients.modifyMolecules().addMonomer(5,1,1);
	ingredients.modifyMolecules().addMonomer(1,5,1);
	ingredients.modifyMolecules().addMonomer(5,5,1);
	ingredients.modifyMolecules().addMonomer(1,1,5);
	
	ingredients.modifyMolecules()[0].setCharge(0);
	ingredients.modifyMolecules()[1].setCharge(1);
	ingredients.modifyMolecules()[2].setCharge(2);
	ingredients.modifyMolecules()[3].setCharge(-1);
	ingredients.modifyMolecules()[4].setCharge(-2);
	
	ingredients.setBjerrumLength(4.0);
	ingredients.setKappa(0.2);
	ingredients.synchronize(ingredients);
	
	EXPECT_NEAR(ingredients.getCurrentDebyePotential(ingredients),-1.97155,0.000005);
	
	
}


TEST_F(TestFeatureDebyeHuckel, ApplyMoveVerletListUpdate)
{
	MyIngredients ingredients;
	ingredients.setBoxX(64);
	ingredients.setBoxY(64);
	ingredients.setBoxZ(64);
	ingredients.setPeriodicX(1);
	ingredients.setPeriodicY(1);
	ingredients.setPeriodicZ(1);

        //once inside box	
	ingredients.modifyMolecules().addMonomer(1,1,1);
	ingredients.modifyMolecules().addMonomer(6,1,1);
	ingredients.modifyMolecules().addMonomer(1,6,1);
	ingredients.modifyMolecules().addMonomer(6,6,1);
	ingredients.modifyMolecules().addMonomer(1,1,12);
	
	ingredients.modifyMolecules()[0].setCharge(1);
	ingredients.modifyMolecules()[1].setCharge(1);
	ingredients.modifyMolecules()[2].setCharge(2);
	ingredients.modifyMolecules()[3].setCharge(-1);
	ingredients.modifyMolecules()[4].setCharge(-2);
	
	ingredients.setBjerrumLength(4.0);
	ingredients.setKappa(0.2);
	ingredients.setCutoffRadius(6.0);
	ingredients.setVerletSkin(4.0);
	ingredients.setUseVerletList(true);
	
	ingredients.synchronize(ingredients);
	
	//distance is 11 lattice units. only the four fixed
	//monomers in list
	EXPECT_EQ(ingredients.getVerletListLength(),12);
	
	MoveLocalSc localmove;
	//initialize localmove for move of mono 4 alon negative z
	while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=4)
		localmove.init(ingredients);
	
	//now the distance is reduced to 10 units
	std::cerr<<"before move to 1 1 11\n";
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),12);
	localmove.apply(ingredients);
	//new position (1,1,11)
	localmove.resetProbability();
	//after one step the list is not updated
	
	
	//distance is reduced to 9 units and the monomer moved two units.
	//the list is rebuild when a check is made after this move
	std::cerr<<"before move to 1 1 10\n";
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),12);
	localmove.apply(ingredients);
	//new position (1,1,10)
	localmove.resetProbability();
	
	
	
	std::cerr<<"before move to 1 1 9\n";
	//now the list is updated, the list becomes longer
	//moved distances reset to 0
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),14);
	//reducing distance to 8 units
	localmove.apply(ingredients);
	localmove.resetProbability();
	
	
	//distance is reduced to 7 units...rebuild list at next check after this move
	std::cerr<<"before move to 1 1 8\n";
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),14);
	localmove.apply(ingredients);
	localmove.resetProbability();
	
	
	//distance moved is again 2, so there is a rebuild of the list
	//the distance is no 7 to the closest mono, 8.6 to the two next closest
	//and 9.9 to the farthest. so all monomers are in the list
	//moved distances reset to 0
	std::cerr<<"before move to 1 1 7\n";
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),20);
	localmove.apply(ingredients);
	localmove.resetProbability();
	
	
}

TEST_F(TestFeatureDebyeHuckel, ApplyMoveVerletListUpdatePeriodic)
{
	MyIngredients ingredients;
	ingredients.setBoxX(64);
	ingredients.setBoxY(64);
	ingredients.setBoxZ(64);
	ingredients.setPeriodicX(1);
	ingredients.setPeriodicY(1);
	ingredients.setPeriodicZ(1);

        //around periodic boundaries	
	ingredients.modifyMolecules().addMonomer(1,1,1);
	ingredients.modifyMolecules().addMonomer(6,1,1);
	ingredients.modifyMolecules().addMonomer(1,6,1);
	ingredients.modifyMolecules().addMonomer(6,6,1);
	ingredients.modifyMolecules().addMonomer(1,1,76);
	
	ingredients.modifyMolecules()[0].setCharge(1);
	ingredients.modifyMolecules()[1].setCharge(1);
	ingredients.modifyMolecules()[2].setCharge(2);
	ingredients.modifyMolecules()[3].setCharge(-1);
	ingredients.modifyMolecules()[4].setCharge(-2);
	
	ingredients.setBjerrumLength(4.0);
	ingredients.setKappa(0.2);
	ingredients.setCutoffRadius(6.0);
	ingredients.setVerletSkin(4.0);
	ingredients.setUseVerletList(true);
	
	ingredients.synchronize(ingredients);
	
	//distance is 9 lattice units. only the four fixed
	//monomers in list
	EXPECT_EQ(ingredients.getVerletListLength(),12);
	

		MoveLocalSc localmove;
	//initialize localmove for move of mono 4 alon negative z
	while(localmove.getDir().getZ()!=-1 ||  localmove.getIndex()!=4)
		localmove.init(ingredients);
	
	//now the distance is reduced to 10 units
	std::cerr<<"before move to 1 1 11\n";
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),12);
	localmove.apply(ingredients);
	//new position (1,1,11)
	localmove.resetProbability();
	//after one step the list is not updated
	
	
	//distance is reduced to 9 units and the monomer moved two units.
	//the list is rebuild when a check is made after this move
	std::cerr<<"before move to 1 1 10\n";
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),12);
	localmove.apply(ingredients);
	//new position (1,1,10)
	localmove.resetProbability();
	
	
	
	std::cerr<<"before move to 1 1 9\n";
	//now the list is updated, the list becomes longer
	//moved distances reset to 0
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),14);
	//reducing distance to 8 units
	localmove.apply(ingredients);
	localmove.resetProbability();
	
	
	//distance is reduced to 7 units...rebuild list at next check after this move
	std::cerr<<"before move to 1 1 8\n";
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),14);
	localmove.apply(ingredients);
	localmove.resetProbability();
	
	
	//distance moved is again 2, so there is a rebuild of the list
	//the distance is no 7 to the closest mono, 8.6 to the two next closest
	//and 9.9 to the farthest. so all monomers are in the list
	//moved distances reset to 0
	std::cerr<<"before move to 1 1 7\n";
	localmove.check(ingredients);
	EXPECT_EQ(ingredients.getVerletListLength(),20);
	localmove.apply(ingredients);
	localmove.resetProbability();
}