/*****************************************************************************/
/**
 * @file
 * @brief Tests for the class FeatureChargedMonomers
 * 
 * @author Martin
 * @date 07.07.2014
 * */
/*****************************************************************************/


#include "gtest/gtest.h"
#include <iostream>
#include <sstream>
#include <LeMonADE/core/Molecules.h>
#include <LeMonADE/core/Ingredients.h>
#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/io/FileImport.h>
#include <LeMonADE/feature/FeatureBondset.h>
#include <LeMonADE/updater/moves/MoveBase.h>
#include <LeMonADE/updater/moves/MoveLocalSc.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFile.h>
#include <LeMonADE/feature/FeatureMoleculesIO.h>

#include <LeMonADE-Extensions/features/FeatureChargedMonomers.h>

using namespace std;

// class with body that is needed for every test, only used by TEST_F()
/*****************************************************************************/
/**
 * @class TestFeatureChargedMonomers
 * @brief checking different classes and functions depending to FeatureChargedMonomers
 * */
/*****************************************************************************/
class TestFeatureChargedMonomers: public ::testing::Test{
protected:
  typedef LOKI_TYPELIST_2(FeatureMoleculesIO,FeatureChargedMonomers) Features;
  typedef ConfigureSystem<VectorInt3,Features> Config;
  typedef Ingredients<Config> MyIngredients;
  
  /* suppress cout output for better readability -->un-/comment here:*/    
public:
  //redirect cout output
  virtual void SetUp(){
    originalBuffer=cout.rdbuf();
    cout.rdbuf(tempStream.rdbuf());
  };
  //restore original output
  virtual void TearDown(){
    cout.rdbuf(originalBuffer);
  };
private:
  std::streambuf* originalBuffer;
  std::ostringstream tempStream;
  /* ** */
};

/*****************************************************************************/
TEST_F(TestFeatureChargedMonomers, CheckChargeTag)
{
  ChargedMonomer testTag;
  EXPECT_EQ(testTag.getCharge(),0);
  testTag.setCharge(3);
  EXPECT_EQ(testTag.getCharge(),3);
  testTag.setCharge(-2);
  EXPECT_EQ(testTag.getCharge(),-2);
}

/*****************************************************************************/
TEST_F(TestFeatureChargedMonomers, CheckReadWrite)
{
  MyIngredients ingredients;
  
  ingredients.setBoxX(64);
  ingredients.setBoxY(64);
  ingredients.setBoxZ(64);
  ingredients.setPeriodicX(true);
  ingredients.setPeriodicY(true);
  ingredients.setPeriodicZ(true);
  
  
  
  ingredients.modifyMolecules().addMonomer(1,1,1);
  EXPECT_EQ(int(ingredients.getMolecules()[0].getCharge()),0);
  ingredients.modifyMolecules()[0].setCharge(-1);
  EXPECT_EQ(int(ingredients.getMolecules()[0].getCharge()),-1);
  
  ingredients.modifyMolecules().addMonomer(2,3,1);
  ingredients.modifyMolecules().addMonomer(3,5,1);
  ingredients.modifyMolecules().addMonomer(4,7,1);
  ingredients.modifyMolecules().addMonomer(10,7,1);
  
  ingredients.modifyMolecules()[0].setCharge(0);
  ingredients.modifyMolecules()[2].setCharge(1);
  ingredients.modifyMolecules()[3].setCharge(1);
  ingredients.modifyMolecules()[4].setCharge(-1);
  
  string filename("tmpTestFeatureChargedMonomers.bfm");
  AnalyzerWriteBfmFile<MyIngredients> writeobject(filename, ingredients);
  writeobject.initialize();
  writeobject.execute();
  
  MyIngredients ingredients2;
  FileImport<MyIngredients> fileImport(filename,ingredients2);
  EXPECT_EQ(int(ingredients.getMolecules()[0].getCharge()),0);
  EXPECT_EQ(int(ingredients.getMolecules()[1].getCharge()),0);
  EXPECT_EQ(int(ingredients.getMolecules()[2].getCharge()),1);
  EXPECT_EQ(int(ingredients.getMolecules()[3].getCharge()),1);
  EXPECT_EQ(int(ingredients.getMolecules()[4].getCharge()),-1);
  remove(filename.c_str());

  MyIngredients ingredients3;
  ingredients3.setBoxX(64);
  ingredients3.setBoxY(64);
  ingredients3.setBoxZ(64);
  ingredients3.setPeriodicX(true);
  ingredients3.setPeriodicY(true);
  ingredients3.setPeriodicZ(true);
  
  ingredients3.modifyMolecules().addMonomer(1,1,1);
  ReadChargedMonomers <MyIngredients> command(ingredients3);
  
  //check reaction to correct standard input
  istringstream inputStandard("\n1-1:1");
  command.setInputStream(&inputStandard);
  EXPECT_NO_THROW(command.execute());
  EXPECT_EQ(int(ingredients3.getMolecules()[0].getCharge()),1);
  
  //check reaction to spacers
  istringstream inputSpacer("\n  1  - 1 :    2   ");
  command.setInputStream(&inputSpacer);
  EXPECT_NO_THROW(command.execute());
  EXPECT_EQ(int(ingredients3.getMolecules()[0].getCharge()),2);
  
  //check reaction to empty input
  istringstream inputEmpty("\n ");
  command.setInputStream(&inputEmpty);
  EXPECT_THROW(command.execute(),std::runtime_error);

  //check reaction to wrong format
  istringstream inputWrong1("\na-b:c");
  command.setInputStream(&inputWrong1);
  EXPECT_THROW(command.execute(),std::runtime_error);
  
  //check reaction to no/wrong seperators(-, :)
  istringstream inputWrong2("\n1:1-2");
  command.setInputStream(&inputWrong2);
  EXPECT_THROW(command.execute(),std::runtime_error);
  
  istringstream inputWrong3("\n1-1-2");
  command.setInputStream(&inputWrong3);
  EXPECT_THROW(command.execute(),std::runtime_error);
  
  ingredients3.modifyMolecules().addMonomer(10,1,1);
  ingredients3.modifyMolecules().addMonomer(20,1,1);
  istringstream inputRight4("\n1-3:1");
  command.setInputStream(&inputRight4);
  EXPECT_NO_THROW(command.execute());
  EXPECT_EQ(int(ingredients3.getMolecules()[0].getCharge()),1);
  EXPECT_EQ(int(ingredients3.getMolecules()[1].getCharge()),1);
  EXPECT_EQ(int(ingredients3.getMolecules()[2].getCharge()),1);
  
  
}
/*****************************************************************************/

TEST_F(TestFeatureChargedMonomers, CheckMove)
{
  MyIngredients ingredients;
  ingredients.modifyMolecules().addMonomer(1,1,1);
  
  MoveBase move;
  EXPECT_TRUE(move.check(ingredients));
  
  MoveLocalSc localmove;
  localmove.init(ingredients);
  EXPECT_TRUE(localmove.check(ingredients));

}

/*****************************************************************************/

TEST_F(TestFeatureChargedMonomers, Synchronize)
{
  MyIngredients ingredients;
  ingredients.setBoxX(64);
  ingredients.setBoxY(64);
  ingredients.setBoxZ(64);
  ingredients.setPeriodicX(true);
  ingredients.setPeriodicY(true);
  ingredients.setPeriodicZ(true);
  ingredients.modifyMolecules().addMonomer(1,1,1);
  ingredients.synchronize(ingredients);
  EXPECT_EQ(0,ingredients.getChargedMonomers().size());
  
  ingredients.modifyMolecules()[0].setCharge(1);
  ingredients.synchronize(ingredients);
  EXPECT_EQ(1,ingredients.getChargedMonomers().size());
  
  ingredients.modifyMolecules().addMonomer(10,1,1);
  ingredients.modifyMolecules()[1].setCharge(-1);
  ingredients.synchronize(ingredients);
  EXPECT_EQ(2,ingredients.getChargedMonomers().size());
  
}