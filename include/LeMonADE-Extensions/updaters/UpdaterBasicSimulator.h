#ifndef BASIC_SIMULATOR_H
#define BASIC_SIMULATOR_H

#include <LeMonADE/updater/AbstractUpdater.h>
#include <LeMonADE/updater/moves/MoveLocalBase.h>

//simple simulation updater. takes the type of move as template 
//argument and the number of mcs to be executed as argument for the 
//constructor

//modified version of UpdaterSimpleSimulator, difference is only that one can
//give it an upper simulation limit

/**
 * @file
 *
 * @class UpdaterBasicSimulator
 *
 * @brief Basic simulation updater for general purpose.
 *
 * @details It takes the type of move as template argument MoveType
 * and the number of mcs to be executed as argument for the constructor
 *
 * @todo remove IngredientsUpdater and use AbstractUpdater
 *
 * @tparam IngredientsType Ingredients class storing all system information( e.g. monomers, bonds, etc).
 * @tparam <MoveType> name of the specialized move.
 */
template<class IngredientsType,class MoveType>
class UpdaterBasicSimulator:public IngredientsUpdater<IngredientsType>
{

public:
  /**
   * @brief Standard Constructor initialized with ref to Ingredients and MCS per cycle
   *
   * @param ing a reference to the IngredientsType - mainly the system
   * @param steps MCS per cycle to performed by execute()
   */
  UpdaterBasicSimulator(IngredientsType& ing,uint32_t steps,uint64_t upperMcsLimit)
  :IngredientsUpdater<IngredientsType>(ing),nsteps(steps),maxMcs(upperMcsLimit)
  {}
  
  /**
   * @brief This checks all used Feature and applies all Feature if all conditions are met.
   *
   * @details This function runs over \a steps MCS and performs the moves.
   * It setting the age of the system and prints a simple simple simulation speed
   * in the number of attempted monomer moves per s (tried and performed monomer moves/s).
   *
   * @return True if function are done.
   */
  bool execute()
  {
	//time_t startTimer = time(NULL); //in seconds
	//std::cout<<"mcs "<<this->getIngredients().getMolecules().getAge() << " passed time " << ((difftime(time(NULL), startTimer)) ) <<std::endl;
	  if(maxMcs<=this->getIngredients().getMolecules().getAge())
		  return false;

    for(int n=0;n<nsteps;n++){
      
	for(int m=0;m<this->getIngredients().getMolecules().size();m++)
	{
		move.init(this->getIngredients());
		
		if(move.check(this->getIngredients())==true)
		{
			move.apply(this->getIngredients());
		}
	}
      
    }
    
    this->getIngredients().modifyMolecules().setAge(this->getIngredients().modifyMolecules().getAge()+nsteps);
    
    //std::cout<<"mcs "<<this->getIngredients().getMolecules().getAge() << " with " << (((1.0*nsteps)*this->getIngredients().getMolecules().size())/(difftime(time(NULL), startTimer)) ) << " [attempted moves/s]" <<std::endl;
    //std::cout<<"mcs "<<this->getIngredients().getMolecules().getAge() << " passed time " << ((difftime(time(NULL), startTimer)) ) << " with " << nsteps << " MCS "<<std::endl;

    return true;
  }
  
  /**
   * @brief This function is called \a once in the beginning of the TaskManager.
   *
   * @details It´s a virtual function for inheritance.
   * Use this function for initializing tasks (e.g. init SDL)
   *
   **/
  virtual void initialize(){};

  /**
   * @brief This function is called \a once in the end of the TaskManager.
   *
   * @details It´s a virtual function for inheritance.
   * Use this function for cleaning tasks (e.g. destroying arrays, file outut)
   *
   **/
  virtual void cleanup(){};

private:
  //! Specialized move to be used
  MoveType move;

  //! Number of mcs to be executed
  uint32_t nsteps;
  
  //! max age of system
  uint64_t maxMcs;
};

#endif
