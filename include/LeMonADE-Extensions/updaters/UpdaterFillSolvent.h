#ifndef UPDATER_FILL_SOLVENT_H
#define UPDATER_FILL_SOLVENT_H

#include <stdint.h>
#include <cmath>
#include <stdlib.h>
#include<stdexcept>
#include<vector>

#include <LeMonADE/updater/UpdaterAbstractCreate.h>
#include <LeMonADE/utility/Vector3D.h>

/*******************************************************************************
 *this updater fills the system with solvent molecules up to a density set
 *either by the constructor argument, or the function setSolventDensity.
 ******************************************************************************/
template<class IngredientsType>
class UpdaterFillSolvent:public UpdaterAbstractCreate<IngredientsType>
{
public:
	UpdaterFillSolvent(IngredientsType& ing,double dens,int32_t type=3)
	:UpdaterAbstractCreate<IngredientsType>(ing),ingredients(ing),density(dens),solventType(type){}
	~UpdaterFillSolvent(){}
	
	bool execute(){return true;}
	void initialize();
	void cleanup(){}
	
	void setSolventDensity(double d){density=d;}
	void setSolventType(int type){solventType=type;}
private:
	
	IngredientsType& ingredients;
	double density;
	int solventType;
};

/*****************************************************************************
 *implementation of members
 ******************************************************************************/

/*****************************************************************************
 *initialize
 ******************************************************************************/
template<class IngredientsType>
void UpdaterFillSolvent<IngredientsType>::initialize()
{
  //first find the number of solvent molecules necessary
  size_t occupiedSites=ingredients.getMolecules().size()*8;
  size_t neededSolvents=static_cast<uint32_t>( (density*double(ingredients.getNumberOfLatticeSites())-double(occupiedSites))/8.0);
  
  //if density is already higher, throw an exception
  if(neededSolvents<0)
    throw std::runtime_error("UpdaterFillSolvent::initialize(): density without solvent is already higher than the requested total density.\n");
  
  size_t solventIndexStart=ingredients.getMolecules().size();
  
  size_t n=0;
  while(n<neededSolvents){
	  
	  if(!UpdaterAbstractCreate<IngredientsType>::addSingleMonomer(solventType)){
		  std::stringstream errormessage;
		  errormessage<<"UpdaterFillSolvent::initialize(). Adding solvent "<<n+1<<" of "<<neededSolvents<<" failed\n";
		  throw std::runtime_error(errormessage.str());
	  }
	  else
		  n++;
  }
  
  ingredients.setCompressedOutputIndices(solventIndexStart,ingredients.getMolecules().size()-1);
  
  
}




#endif /*FILL_SOLVENT_UPDATER_H*/
