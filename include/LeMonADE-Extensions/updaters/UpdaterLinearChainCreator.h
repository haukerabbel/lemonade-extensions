#ifndef UPDATER_LINEAR_CHAIN_CREATOR_H
#define UPDATER_LINEAR_CHAIN_CREATOR_H

#include<vector>
#include<map>

#include <LeMonADE/updater/AbstractUpdater.h>
#include <LeMonADE/updater/moves/MoveLocalBase.h>
#include <LeMonADE/updater/moves/MoveLocalSc.h>
#include <LeMonADE/updater/moves/MoveAddMonomerSc.h>
#include <LeMonADE/utility/RandomNumberGenerators.h>

/**
 * @file conains class UpdaterLinearChainCreator
 * 
 * @class UpdaterLinearChainCreator
 * @brief Updater for creating one or more linear chains of equal length.
 * @details
 * */

template<class IngredientsType> 
class UpdaterLinearChainCreator:public AbstractUpdater
{
public:

	//! constructor
	UpdaterLinearChainCreator(IngredientsType& ing,
				  size_t chainLength,
			   uint32_t nChains=1,
			   bool globalRelax=false);
	
	UpdaterLinearChainCreator(IngredientsType& ing,
				  std::vector<int32_t> monoTypes,
			   uint32_t nChains=1,
			   bool globalRelax=false);
	//! destructor
	virtual ~UpdaterLinearChainCreator();
	
	//! specify length and mono type of chains 
	void resize(size_t chainlength,int32_t monoType=1);
	
	//! specify length and mono type of chains 
	void resize(std::vector<int32_t> monoTypes);
	
	//! specify number of chains
	void setNChains(uint32_t n){nChains=n;}
	
	//! specify maximum number of attempts when adding a monomer to the chain
	void setMaxAttempts(uint32_t m){maxAttempts=m;}
	
	//! change mode of sytem relaxation, which may be necessary during creation 
	void setGlobalRelaxation(bool val){globalRelaxation=val;}
	
	//! initialize, see AbstractUpdater and TaskManager
	virtual void initialize();
	
	//! creates the chains
	virtual bool execute(){}
	
	//! cleanup, see AbstractUpdater and TaskManager
	virtual void cleanup(){}
	
private:
	//! reference to simulation system
	IngredientsType& ingredients;
	
	//! for getting random numbers during system setup
	RandomNumberGenerators randomNumbers;
	
	//! number of chains to be created
	uint32_t nChains;
	
	//! series of monomer types to be used for chains
	std::vector<int32_t> monomerType;
	
	//! saves the monomer indices of newly created chains for book keeping during setup
	std::vector<size_t> chainstarts;
	
	//! maximum number of attempts when adding a single monomer
	uint32_t maxAttempts;
	
	//! tag for deciding on relaxation mode during setup
	bool globalRelaxation;
	
	//! add a chain to the system. used by execute()
	bool addSingleChain();
	
	//! start a new chain. used by addSingleChain()
	bool addNewChainstart(int32_t type);
	
	//! continue growing current chain. used by addSingleChain()
	bool addMonomerToChain(int32_t type);
	
	//! draw a random bond from the bondset
	VectorInt3 drawRandomBond();
	
	//! simulate the system, or the currently growing chain, for nSteps MCS
	void simulate(uint32_t nSteps);
	
	//! fold back a coordinate value into the box of size boxSize 
	int32_t foldBack(int32_t value,int32_t boxSize) const;
	
	//this nested class describes a local sc bfm move, but the monomer
	//indices can be restricted to a certain range
	//! nested class defining a restricted local move, used for local relaxation
	class RestrictedLocalMove:public MoveLocalSc{
	public:
		RestrictedLocalMove():minIndex(0),maxIndex(0){}
		void setIndexRange(uint32_t min,uint32_t max){minIndex=min;maxIndex=max;}
		
		template<class IngredientsTypeMove>
		void init(const IngredientsTypeMove& ing){
			//random init first
			this->MoveLocalSc::init(ing);
			//then reset the index to random within allowed range
			uint32_t actualMax=std::max(ing.getMolecules().size(),maxIndex);
			uint32_t randomOffset=this->randomNumbers.r250_rand32()%(actualMax-minIndex);
			this->setIndex(minIndex+randomOffset);
		}
	private:
		uint32_t minIndex;
		uint32_t maxIndex;
		
	};
};




////////////////////////////////////////////////////////////////////////////////
/**
 * @brief Constructor
 * @param ing system to create chains in
 * @param chainlength length of the chains to be created
 * @param nchains number of chains to be created
 * @param globalRelax sets variable globalRelaxation. defaults to false. see setGlobalRelaxation(bool)
 * */
template<class IngredientsType>
UpdaterLinearChainCreator<IngredientsType>::UpdaterLinearChainCreator(IngredientsType& ing,
								      size_t chainlength, 
								      uint32_t nchains, 
								      bool globalRelax)
:ingredients(ing)
,maxAttempts(100)
,nChains(nchains)
,globalRelaxation(globalRelax)
{
	//this contains the types of monomers in the chain. set to default value
	monomerType.resize(chainlength,1);
	chainstarts.resize(0);
}

////////////////////////////////////////////////////////////////////////////////
/**
 * @brief Constructor
 * @param ing system to create chains in
 * @param monoTypes vector representing the sequence of monomer types in the polymer
 * @param nchains number of chains to be created
 * @param globalRelax sets variable globalRelaxation. defaults to false. see setGlobalRelaxation(bool)
 * */
template<class IngredientsType>
UpdaterLinearChainCreator<IngredientsType>::UpdaterLinearChainCreator(IngredientsType& ing,
								      std::vector<int32_t> monoTypes, 
								      uint32_t nchains, 
								      bool globalRelax)
:ingredients(ing)
,maxAttempts(100)
,nChains(nchains)
,globalRelaxation(globalRelax)
,monomerType(monoTypes)
{
	chainstarts.resize(0);
}

////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
UpdaterLinearChainCreator<IngredientsType>::~UpdaterLinearChainCreator()
{
}


////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void UpdaterLinearChainCreator<IngredientsType>::resize(size_t chainlength, int32_t monoType)
{
	monomerType.resize(chainlength,monoType);
}

////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void UpdaterLinearChainCreator<IngredientsType>::resize(std::vector<int32_t> monoTypes)
{
	monomerType=monoTypes;
}

////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void UpdaterLinearChainCreator<IngredientsType>::initialize()
{
	bool success=true;
	
	for(uint32_t n=0;n<nChains;n++){
		
		std::cout<<"creating chain no "<<n<<std::endl;
		success&=addSingleChain();
		
		if(success==false){
			
			std::stringstream errormessage;
			errormessage<<"UpdaterLinearChainCreator::initialize()\n"
			<<"could not create chain no "<<n<<" out of "<<nChains
			<<std::endl;
			
			throw std::runtime_error(errormessage.str());
		}	
	}
	
}

////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
bool UpdaterLinearChainCreator<IngredientsType>::addSingleChain()
{
	bool success=addNewChainstart(monomerType[0]);
	if(success==false) return false;
	
	for(uint32_t n=1;n<monomerType.size();n++){

		success=addMonomerToChain(monomerType[n]);
		if(success==false) break;
	}
	
	return success;
}

////////////////////////////////////////////////////////////////////////////////

template<class IngredientsType>
bool UpdaterLinearChainCreator<IngredientsType>::addNewChainstart(int32_t type)
{
	//get the box size, because we want to place the chainstarts inside the box
	int32_t boxX=ingredients.getBoxX();
	int32_t boxY=ingredients.getBoxY();
	int32_t boxZ=ingredients.getBoxZ();
	
	//monomers are added using the MoveAddScMonomer
	MoveAddMonomerSc addMonomer;
	//the type is used only if FeatureAttributes is used.
	addMonomer.setTag(type); 
	
	//draw random starting position. stop after maxAttempts attempts,
	//if not successful until then
	bool success=false;
	int32_t x,y,z;
	
	for(size_t n=0;n<maxAttempts;n++){
		
		x=foldBack(int32_t(randomNumbers.r250_rand32()),boxX);
		y=foldBack(int32_t(randomNumbers.r250_rand32()),boxY);
		z=foldBack(int32_t(randomNumbers.r250_rand32()),boxZ);
		
		addMonomer.init(ingredients);
		addMonomer.setPosition(x,y,z);
		
		success=addMonomer.check(ingredients);
		if(success) break;
	}
	
	//in case of success, add the monomer and return true, otherwise return false
	if(success){
		chainstarts.push_back(ingredients.getMolecules().size());
		addMonomer.apply(ingredients);
		return true;
	}
	else return false;	
}


////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
bool UpdaterLinearChainCreator<IngredientsType>::addMonomerToChain(int32_t type)
{
	//this is used to determine which monomer to connect to
	size_t currentMoleculesSize=ingredients.getMolecules().size();
	
	//monomers are added using MoveAddScMonomer
	MoveAddMonomerSc addMonomer;
	//the type is used only if FeatureAttributes is used.
	addMonomer.setTag(type);
	
	//for maxAttempts attempts, draw a bondvector from the bondset
	//and check if a new monomer can be inserted into the position
	bool success=false;
	
	for(size_t n=0;n<maxAttempts;n++){
		
		//position of last monomer in the system
		VectorInt3 previousPosition=ingredients.getMolecules()[currentMoleculesSize-1];
		//get random bond vector from bondset (in case of standard bfm, this uses a reduced bondset)
		VectorInt3 bondVector=drawRandomBond();
		//initialize the move and check for possible insertion
		addMonomer.init(ingredients);
		addMonomer.setPosition(bondVector+previousPosition);
		success=addMonomer.check(ingredients);
		
		//in case of success, add the monomer and connect it
		if(success==true){
			addMonomer.apply(ingredients);			
			ingredients.modifyMolecules().connect(currentMoleculesSize-1,currentMoleculesSize);
			break;
		}
		//if not successful after 20 attempts, the system is simulated
		//for 50 mcs. normally only the chain being constructed is 
		//moved, unless the flag globalRelaxation is set to true.
		else if(success==false && n%20==0) simulate(50);
		
	}
	
	return success;
}

////////////////////////////////////////////////////////////////////////////////
/**
 * @brief simulate the system, or a part of it, for nSteps MCS
 * @details this function is used to simulate the system, in case no free
 * position for adding a monomer to the chain could be found. if the flag
 * globalRelaxation==false (default), only the monomers belonging to the 
 * currently constructed chain are moved for relaxation. otherwise, the complete
 * system is simulated. 
 * */
template<class IngredientsType>
void UpdaterLinearChainCreator<IngredientsType>::simulate(uint32_t nSteps)
{
	//these indexes determine the range of monomer indexes within
	//molecules, which are taken into account for the relaxation
	uint32_t simulateIndexStart;
	uint32_t simulateNMonomers;
	if(globalRelaxation==true)
		simulateIndexStart=0;
	else
		simulateIndexStart=*(chainstarts.rbegin());
	
	//number of monomers to be simulated
	simulateNMonomers=ingredients.getMolecules().size()-simulateIndexStart;
	
	//using a RestrictedLocalMove to move only the desired set of monomers
	RestrictedLocalMove move;
	move.setIndexRange(simulateIndexStart,ingredients.getMolecules().size()-1);
	
	
#ifdef DEBUG	
	std::cout<<"UpdaterLinearChainCreator: simulating "<<nSteps*simulateNMonomers
	<<" to relax configuration...";
#endif
	//now simulate the (sub-)set of monomers
	for(uint32_t n=0;n<nSteps*simulateNMonomers;n++){
		
		//initialize the move. this calls the init function of the
		//nested class RestrictedLocalMove
		move.init(ingredients);
		
		//for checking and applying the move, it must be cast back
		//to MoveLocalSc, because only this move is implemented in 
		//the features. otherwise, the check would always return true
		MoveLocalSc castMove=static_cast<MoveLocalSc>(move);
		if(castMove.check(ingredients)==true)
			castMove.apply(ingredients);
	}
	
#ifdef DEBUG
	std::cout<<"done\n";
#endif
}

////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
VectorInt3 UpdaterLinearChainCreator<IngredientsType>::drawRandomBond()
{
	//get the currently used set of bond vectors
	uint32_t bondsetSize=ingredients.getBondset().size();
	
	//if the bondset is empty, exit
	if(bondsetSize==0){
		std::stringstream errormessage;
		errormessage<<"UpdaterLinearChainCreator::drawRandomBond():\n"
		<<"no bonds in bondset\n";
		throw std::runtime_error(errormessage.str());
	}
	
	//here we draw a random bond off the bondset. we exclude 
	//bonds with length >=sqrt(6), which makes sense for standard bfm
	//drawing a bond is tried for maxAttempts attempts.
	uint32_t attempts=0;
	bool partOfReducedBondset=false;
	VectorInt3 bondVector;
	
	do{
		//get random uint in range 0...bondsetSize
		uint32_t randomBond=(randomNumbers.r250_rand32())%bondsetSize;
		
		//get the corresponding bond vector
		std::map<int32_t,VectorInt3>::const_iterator it=ingredients.getBondset().begin();
		for(uint32_t n=0;n<randomBond;n++)
			++it;
		
		bondVector=it->second;
		
		//check if the vector is part of the reduced bondset 
		partOfReducedBondset=( bondVector*bondVector < 6);
		
		attempts++;
		
	}while(partOfReducedBondset==false||attempts<maxAttempts);
	
	//if we have drawn a valid vector, return it, otherwise exit
	if(partOfReducedBondset==true)
		return bondVector;
	else{
		std::stringstream errormessage;
		errormessage<<"UpdaterLinearChainCreator::drawRandomBond()\n"
		<<"could not find a bond vector after "<<maxAttempts<<" attempts\n";
		throw std::runtime_error(errormessage.str());
	}
}

////////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
int32_t UpdaterLinearChainCreator<IngredientsType>::foldBack(int32_t value,int32_t boxSize) const
{
	return (((value%boxSize)+boxSize)%boxSize);
}

#endif /*UPDATER_LINEAR_CHAIN_CREATOR_H*/