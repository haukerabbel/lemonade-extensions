#ifndef UNMOVEDMONOMERSANALYZER_H
#define UNMOVEDMONOMERSANALYZER_H

#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/analyzer/AbstractAnalyzer.h>

/******************************************************************************
 *this analyzer checks if all monomers have been moved.
 ******************************************************************************/
template<class IngredientsType>
class UnmovedMonomersAnalyzer:public AbstractAnalyzer
{
public:
    UnmovedMonomersAnalyzer(const IngredientsType& ing)
        :ingredients(ing)
        {
		
	}

    virtual ~UnmovedMonomersAnalyzer(){ if(hasMoved!=0) delete [] hasMoved;}


    virtual bool execute();
    virtual void initialize();
    virtual void cleanup(){}
private:

    const IngredientsType& ingredients;
    typename IngredientsType::molecules_type molecules_first_step;
    bool* hasMoved;
};


/************ implementation ******************/

template<class IngredientsType>
bool UnmovedMonomersAnalyzer<IngredientsType>::execute()
{
    if(molecules_first_step.size()!=ingredients.getMolecules().size())
        molecules_first_step=ingredients.getMolecules();

    if(ingredients.getMolecules().getAge()==molecules_first_step.getAge())
        return true;


    const typename IngredientsType::molecules_type molecules=ingredients.getMolecules();
    std::vector<size_t> unmovedMonos;

    for (size_t i=0; i < molecules.size();++i)
    {
        VectorInt3 diff = molecules[i] - molecules_first_step[i];
        if ( diff.getLength() <= 5 && hasMoved[i]==false ) unmovedMonos.push_back(i);
	else hasMoved[i]=true;

    }

    if (unmovedMonos.size() >0)
    {
        std::cerr<< "WARNING: numberOfUnmovedMonos in mcs "<<molecules.getAge()<<": " << unmovedMonos.size() << std::endl;
        std::cerr<<"Indices of monomers that have moved less than 5 lattice sites: ";

        for (size_t i=0; i < unmovedMonos.size();++i)
        {
            std::cerr<<unmovedMonos[i]<<" ";
        }
        std::cerr<<std::endl;

    }


    return true;
}

template<class IngredientsType>
void UnmovedMonomersAnalyzer<IngredientsType>::initialize()
{
    molecules_first_step=ingredients.getMolecules();
    hasMoved=new bool[molecules_first_step.size()];
		for(size_t i=0;i<molecules_first_step.size();i++)
		{
			hasMoved[i]=false;
		}

}


#endif // UNMOVEDMONOMERSANALYZER_H
