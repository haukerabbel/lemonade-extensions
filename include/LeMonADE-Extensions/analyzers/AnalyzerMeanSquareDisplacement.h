#ifndef ANALYZER_MEAN_SQUARE_DISPLACEMENT_H
#define ANALYZER_MEAN_SQUARE_DISPLACEMENT_H


#include<iostream>
#include <vector>

#include <LeMonADE/analyzer/AbstractAnalyzer.h>
#include <LeMonADE/utility/ResultFormattingTools.h>
#include <LeMonADE/utility/RandomNumberGenerators.h>

#include <LeMonADE-Extensions/utilities/Moments.h>

/************************************************************************
 * analyzer calculating the msd of a set of particles.
 * (if you use too many particles, it may blow the memory)
 * 
 * Depending on the constructor used, the analyzer will calculate either
 * g3 (i.e. the center of mass MSD of a given set of molecules), or 
 * the direct MSD of a group of monomers (i.e. averaged over every single
 * monomer in a defined set). if the complete Molecules-object
 * (Ingredients::getMolecules() ) is used, the drift of the system is 
 * calculated (MSD of the center of mass of the system).
 * 
 * The user has the option of choosing timestep increment by multiplication
 * (default) or linear increment for the results.
 * 
 * Also, the user can choose to use more than one reference point for the
 * calculation for better statistics (default: a new reference point is
 * added every 100 frames)
 * 
 * Outputfile: msd.dat
 * *********************************************************************/

/***********************************************************************
 * DEFINITION OF THE CLASS
 * ********************************************************************/

/**
 * @file
 *
 * @class AnalyzerMeanSquareDisplacement
 *
 * @brief Analyzer for evaluating the MSD
 *
 *
 *
 * @deprecated
 *
 * @tparam IngredientsType Ingredients class storing all system information( e.g. monomers, bonds, etc).
 *
 *
 *
 * @todo we should reconsider this approach for usability
 *
 * @todo Doxygen-Style
 */
template<class IngredientsType>
class AnalyzerMeanSquareDisplacement:public AbstractAnalyzer
{
public:

	AnalyzerMeanSquareDisplacement(const IngredientsType& ing,std::vector<MonomerGroup<typename IngredientsType::molecules_type> >& chains,std::string fileAppendix="");

	
	//this function only saves the conformations
	virtual bool execute();
	//this function calculates the msd and prints the results to a file
	virtual void cleanup();
	
	  /**
	   * @brief This function is called \a once in the beginning of the TaskManager.
	   *
	   * @details It´s a virtual function for inheritance.
	   * Use this function for initializing tasks (e.g. init SDL)
	   *
	   **/
	  virtual void initialize();

	void setTimeIncrementFactor(double value){useTimeIncrementLinear=false;timeIncrementFactor=value;}
	void setTimeIncrementLinear(uint64_t value){useTimeIncrementLinear=true;timeIncrementLinear=value;}
	
	void setReferencePointIncrement(uint64_t value){startPointIncrement=value;}
  
private:
	//typedef for type of a conformation
	typedef typename IngredientsType::molecules_type molecules_type;

	//contains the complete system information
	const IngredientsType& system;
	//groups of molecules to be analyzed
	const std::vector<MonomerGroup<molecules_type> >& groups;
	
	//the positions and times are stored here
	std::vector<uint64_t> mcsVector;
	std::vector <std::vector < VectorDouble3 > > coordinateVectors;

	//these are tags for the evaluation:
	bool useCOM;  //center of mass msd, when 3rd or 4th constructor is used
	bool useTimeIncrementLinear; //default false
	
	double timeIncrementFactor; //default 1.1
	uint64_t timeIncrementLinear; //only used if explicitly set
	
	//take a new reference point every for better statistics. 
	//if set to zero, only first frame is reference. defaults to 100
	uint64_t startPointIncrement;
	
	std::string fileAppendix_;
 
	VectorDouble3 centerOfMass(const MonomerGroup<molecules_type>& group) const;
};

/*****************************************************************************
 * IMPLEMENTATION OF MEMBERS
 * ***************************************************************************/


/*****************************************************************************
 * CUNSTRUCTOR 3
 * ***************************************************************************/
//if this constructor is used (with a vector of groups as argument), the MSD of 
//the center of mass of the groups is calculated (g3). 

template<class IngredientsType>
AnalyzerMeanSquareDisplacement<IngredientsType>::AnalyzerMeanSquareDisplacement(const IngredientsType& ing,std::vector<MonomerGroup<typename IngredientsType::molecules_type> >& chains,std::string fileAppendix)
	:system(ing),groups(chains),useCOM(true)
	,timeIncrementFactor(1.1),startPointIncrement(100),useTimeIncrementLinear(false)
	,fileAppendix_(fileAppendix)
{
	
}

template<class IngredientsType>
void AnalyzerMeanSquareDisplacement<IngredientsType>::initialize()
{
	coordinateVectors.resize((groups.size()));
}
/*****************************************************************************
 * EXECUTE()
 * ***************************************************************************/
//the execute routine only saves the current positions to coordinateVectors. 
//The calculation happens in the cleanup 
template<class IngredientsType>
bool AnalyzerMeanSquareDisplacement<IngredientsType>::execute()
{
	mcsVector.push_back(system.getMolecules().getAge());
		
	for(size_t n=0;n<groups.size();n++)
	{
		coordinateVectors[n].push_back(centerOfMass(groups[n]));
	}
	
	return true;
}


/*****************************************************************************
 * CLEANUP()
 * ***************************************************************************/
//here the msd is calculated and the results are written to a file

template<class IngredientsType>
void AnalyzerMeanSquareDisplacement<IngredientsType>::cleanup()
{
	//for savety
	if((!useTimeIncrementLinear) && (timeIncrementFactor<1.0) )
		throw std::runtime_error("AnalyzerMeanSquareDisplacement::cleanup: timeIncrementFactor must be larger than 1.0");
	if( (useTimeIncrementLinear) && (timeIncrementLinear<1) )
		throw std::runtime_error("AnalyzerMeanSquareDisplacement::cleanup: timeIncrementLinear must be larger than 1");
	
	
	//this will contain the msd as a funtion of time
	std::vector< std::vector <double> > msd;
	
	//this keeps the values for each timestep. the key of the map are the delta_t. 
	//the second template argument Moments lets you
	//calculate averages and moments of the values for each delta_t
	std::map<uint64_t,Moments<double> > collectedValues; 
	std::map<uint64_t,Moments<double> > collectedValues_x; 
	std::map<uint64_t,Moments<double> > collectedValues_y; 
	std::map<uint64_t,Moments<double> > collectedValues_z; 
	
	
	//used for processing
	uint64_t timestamp,timeInterval;
	
	
	double squareDisplacement;
	double squareDisplacement_x;
	double squareDisplacement_y;
	double squareDisplacement_z;
	
	uint64_t nConformations=mcsVector.size();
	
	std::cout<<"AnalyzerMeanSquareDisplacement::cleanup(). Saved "<<nConformations<<" conformations...processing results\n";

	
	/////////////////////////////////////////////////////////////////////
	//here is where the calculation starts
	
	
	//loop over all the reference points
	uint64_t t_0=0;
	while(t_0<nConformations)
	{

		timestamp=mcsVector[t_0];
		
		//loop over all the delta_t we want to use
		uint64_t dt=1;
		while(dt<mcsVector.size()- t_0)
		{
			//translate the counter to actual mcs. counter goes from
			//1,2,4,8,... while the mcs may be 5000,10000,20000,40000,... (if dt-increment is by multiplication)
			timeInterval=mcsVector[t_0+dt]-timestamp;

			//now loop over all the monomers or molecules we have chosen and calculate
			//their square displacement for this delta_t
			for(size_t groupIndex=0; groupIndex<groups.size(); ++groupIndex)
			{
				VectorDouble3 displacementVector=(coordinateVectors[groupIndex][t_0+dt]-coordinateVectors[groupIndex][t_0]);
				
				squareDisplacement=displacementVector*displacementVector;
				squareDisplacement_x=displacementVector.getX()*displacementVector.getX();
				squareDisplacement_y=displacementVector.getY()*displacementVector.getY();
				squareDisplacement_z=displacementVector.getZ()*displacementVector.getZ();
				
				collectedValues[timeInterval].add(squareDisplacement);
				collectedValues_x[timeInterval].add(squareDisplacement_x);
				collectedValues_y[timeInterval].add(squareDisplacement_y);
				collectedValues_z[timeInterval].add(squareDisplacement_z);
			}
			
			if(useTimeIncrementLinear)
				dt+=timeIncrementLinear;
			else
				dt=static_cast<size_t>(ceil(double(dt)*timeIncrementFactor));
		}
		
		if(startPointIncrement==0) 
			t_0+=mcsVector.size();
		else
			t_0+=startPointIncrement;
	}
	
	//now evaluate the averages and put the results into the vector msd,
	//such that the results can be written into a file by 
	//ResultFormattingTools::writeResultFile
	std::cout<<"calculate averages...\n";
	msd.resize(9);
	
	//put the averages into the vector msd
	std::map<uint64_t,Moments<double> >::iterator it;
	for(it=collectedValues.begin();it!=collectedValues.end();++it)
	{
		//it->first contains the delta_t
		msd[0].push_back(double(it->first));
		//it->seconds are the moments, m_1() being the first moment,i.e. average
		msd[1].push_back(it->second.m_1());
		//mu_2() gives the variance, m_0() the number of samples, i.e sqrt(mu_2/m_0) 
		//an estimate for the error of the mean
		msd[2].push_back(sqrt(it->second.mu_2()/double(it->second.m_0())));
	}
	for(it=collectedValues_x.begin();it!=collectedValues_x.end();++it)
	{
		//it->seconds are the moments, m_1() being the first moment,i.e. average
		msd[3].push_back(it->second.m_1());
		//mu_2() gives the variance, m_0() the number of samples, i.e sqrt(mu_2/m_0) 
		//an estimate for the error of the mean
		msd[4].push_back(sqrt(it->second.mu_2()/double(it->second.m_0())));
	}
	for(it=collectedValues_y.begin();it!=collectedValues_y.end();++it)
	{
		//it->seconds are the moments, m_1() being the first moment,i.e. average
		msd[5].push_back(it->second.m_1());
		//mu_2() gives the variance, m_0() the number of samples, i.e sqrt(mu_2/m_0) 
		//an estimate for the error of the mean
		msd[6].push_back(sqrt(it->second.mu_2()/double(it->second.m_0())));
	}
	for(it=collectedValues_z.begin();it!=collectedValues_z.end();++it)
	{
		//it->seconds are the moments, m_1() being the first moment,i.e. average
		msd[7].push_back(it->second.m_1());
		//mu_2() gives the variance, m_0() the number of samples, i.e sqrt(mu_2/m_0) 
		//an estimate for the error of the mean
		msd[8].push_back(sqrt(it->second.mu_2()/double(it->second.m_0())));
	}
	
	//comment that will be written to the file
	std::stringstream comment;
	comment<<"Created by AnalyzerMeanSquareDisplacement\n";
	comment<<"sample size: "<<groups.size()<<" monomers/molecules\n";
	
	comment<<"total number of conformations saved: "<<nConformations<<std::endl;
	if(startPointIncrement>0) comment<<"for averaging a new reference point was used every "<<startPointIncrement<<" conformations\n";
	comment<<"format: mcs\t <MSD>\tdelta<MSD>\t <MSD_x>\tdelta<MSD_x>\t <MSD_y>\tdelta<MSD_y>\t <MSD_z>\tdelta<MSD_z>\n";
	//write the results to a file. the function ResultFormattingTools::writeResultFile
	//adds a header with informations about the analyzed system, used features, etc
	//to the output file.
	std::string filename = fileAppendix_+"_msd.dat";
	ResultFormattingTools::writeResultFile(filename,system,msd,comment.str());
	
}

template<class IngredientsType>
VectorDouble3 AnalyzerMeanSquareDisplacement<IngredientsType>::centerOfMass(const MonomerGroup<molecules_type>& group) const
{
	VectorDouble3 COM(0.0,0.0,0.0);
	for(size_t n=0;n<group.size();n++)
		COM+=group[n];
		
	COM=COM/double(group.size());
	
// 	//second iteration
	VectorDouble3 COM_corrected(0.0,0.0,0.0);
	int32_t N=0;
	for(size_t n=0;n<group.size();n++){
		if(std::fabs(COM.getZ()-double(group[n].getZ()))<system.getBoxZ()/2.0){
			COM_corrected+=group[n];
			N++;
		}
	}
	
	return COM_corrected/double(N);
	
}
#endif /*MSD_ANALYZER_H*/
