#ifndef ANALYZER_PERFORMANCE_H
#define ANALYZER_PERFORMANCE_H

#include<ctime>
#include<string>
#include<sstream>
#include<vector>

#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/ResultFormattingTools.h>
#include <LeMonADE/analyzer/AbstractAnalyzer.h>

/******************************************************************************
 *this analyzer checks if all monomers have been moved.
 ******************************************************************************/
template<class IngredientsType>
class AnalyzerPerformance:public AbstractAnalyzer
{
public:
    AnalyzerPerformance(const IngredientsType& ing,std::string outfile=std::string("performance.dat"),bool verb=false)
        :ingredients(ing),prevAge(0),verbose(verb),movesPerSecondValues(2),filename(outfile)
	{}

    virtual ~AnalyzerPerformance(){}


    virtual bool execute();
    virtual void initialize();
    virtual void cleanup();
private:

    const IngredientsType& ingredients;
    uint64_t prevAge;
    time_t prevTime;
    bool verbose;
    std::vector< std::vector<double> > movesPerSecondValues;
    std::string filename;
};


/************ implementation ******************/

template<class IngredientsType>
bool AnalyzerPerformance<IngredientsType>::execute()
{
	uint64_t currentAge=ingredients.getMolecules().getAge();
	time_t currentTime=time(NULL);
	double deltaT=difftime(currentTime,prevTime);
	
	if(currentAge==prevAge && verbose){if(verbose)  std::cout<<"AnalyzerPerformance: no change in age. Skipping\n";}
	else if(deltaT<10.0){if(verbose) std::cout<<"AnalyzerPerformance: time difference too small. Skipping\n";}
	else if(prevAge==0){
		std::cout<<"AnalyzerPerformance: age=0. Skipping first step\n";
		prevAge=currentAge;
		prevTime=currentTime;
	}
	else{
		
		double attempedMovesPerSecond=double((currentAge-prevAge)*ingredients.getMolecules().size())/deltaT;
		
		if(verbose) std::cout<<"AnalyzerPerformance: "<<attempedMovesPerSecond<<"att. moves/s\n";
		movesPerSecondValues[0].push_back(currentAge);
		movesPerSecondValues[1].push_back(attempedMovesPerSecond);
			
		prevAge=currentAge;
		prevTime=currentTime;
	}

	return true;
}

template<class IngredientsType>
void AnalyzerPerformance<IngredientsType>::initialize()
{
	prevAge=time(NULL);
}

template<class IngredientsType>
void AnalyzerPerformance<IngredientsType>::cleanup()
{
	std::stringstream comment;
	comment<<"#created by AnalyzerPerformance. Format: mcs, att. moves/s\n";
	ResultFormattingTools::writeResultFile(filename,ingredients,movesPerSecondValues,comment.str());
	
}


#endif // UNMOVEDMONOMERSANALYZER_H
