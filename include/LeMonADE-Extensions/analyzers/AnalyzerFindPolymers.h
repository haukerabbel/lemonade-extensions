#ifndef ANALALYZER_FIND_POLYMERS_H
#define ANALALYZER_FIND_POLYMERS_H

#include<vector>

#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>
#include <LeMonADE/analyzer/AbstractAnalyzer.h>

/******************************************************************************
 * this analyzer checks if all monomers have been moved.
 ******************************************************************************/
template<class IngredientsType>
class AnalyzerFindPolymers:public AbstractAnalyzer
{
	typedef MonomerGroup<typename IngredientsType::molecules_type> group_type;

public:
	AnalyzerFindPolymers(const IngredientsType& ing,
			     std::vector<group_type>& groups,
			     std::vector<int32_t> types)
	:ingredients(ing),groupVector(groups),attributes(types){}
	
	virtual ~AnalyzerFindPolymers(){}
	
	virtual bool execute(){}
	virtual void initialize();
	virtual void cleanup(){}
	
private:
		
	const IngredientsType& ingredients;
	std::vector<group_type>& groupVector;
	std::vector<int32_t> attributes;
};
		
		
/************ implementation ******************/

template<class IngredientsType>
void AnalyzerFindPolymers<IngredientsType>::initialize()
{
	fill_connected_groups(ingredients.getMolecules(),
			      groupVector,
		       group_type(ingredients.getMolecules()),hasType(attributes));
}
		
		
#endif // ANALALYZER_FIND_POLYMERS_H
		