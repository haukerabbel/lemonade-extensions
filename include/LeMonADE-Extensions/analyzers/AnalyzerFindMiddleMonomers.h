#ifndef ANALALYZER_FIND_MIDDLE_MONOMERS_H
#define ANALALYZER_FIND_MIDDLE_MONOMERS_H

#include<vector>

#include <LeMonADE/utility/Vector3D.h>
#include <LeMonADE/utility/MonomerGroup.h>
#include <LeMonADE/utility/DepthIterator.h>
#include <LeMonADE/utility/DepthIteratorPredicates.h>
#include <LeMonADE/analyzer/AbstractAnalyzer.h>

/******************************************************************************
 * this analyzer checks if all monomers have been moved.
 ******************************************************************************/
template<class IngredientsType>
class AnalyzerFindMiddleMonomers:public AbstractAnalyzer
{
	typedef MonomerGroup<typename IngredientsType::molecules_type> group_type;

public:
	AnalyzerFindMiddleMonomers(const IngredientsType& ing,
			     std::vector<group_type>& groups,
			     std::vector<int32_t> types=std::vector<int32_t>(1,1))
	:ingredients(ing),groupVector(groups),attributes(types){}
	
	virtual ~AnalyzerFindMiddleMonomers(){}
	
	virtual bool execute(){}
	virtual void initialize();
	virtual void cleanup(){}
	
private:
		
	const IngredientsType& ingredients;
	std::vector<group_type>& groupVector;
	std::vector<int32_t> attributes;
};
		
		
/************ implementation ******************/

template<class IngredientsType>
void AnalyzerFindMiddleMonomers<IngredientsType>::initialize()
{
	std::vector<group_type> polymers;
	fill_connected_groups(ingredients.getMolecules(),
			      polymers,
		       group_type(ingredients.getMolecules()),hasType(attributes));
	
	for(size_t n=0;n<polymers.size();n++){
		groupVector.push_back(group_type(ingredients.getMolecules()));
		size_t groupSize=polymers[n].size();
		if(groupSize==1) groupVector.rbegin()->push_back(polymers[n].trueIndex(0));
		else if(groupSize%2==0){
			size_t idx=groupSize/2;
			groupVector.rbegin()->push_back(polymers[n].trueIndex(idx-1));
			groupVector.rbegin()->push_back(polymers[n].trueIndex(idx));
		}
		else{
			size_t idx=groupSize/2;
			groupVector.rbegin()->push_back(polymers[n].trueIndex(idx-1));
			groupVector.rbegin()->push_back(polymers[n].trueIndex(idx));
			groupVector.rbegin()->push_back(polymers[n].trueIndex(idx+1));
		}
	}
}
		
		
#endif // ANALALYZER_FIND_MIDDLE_MONOMERS_H
		