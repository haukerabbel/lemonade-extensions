#ifndef FEATURE_DEBYEHUCKEL_H
#define FEATURE_DEBYEHUCKEL_H


#include <LeMonADE/feature/FeatureBoltzmann.h>
#include <LeMonADE/io/FileImport.h>
#include <LeMonADE/utility/DistanceCalculation.h>

#include <LeMonADE-Extensions/features/FeatureChargedMonomers.h>

////////////////////////////////////////////////////////////////////////////////
/****** read and write routines ***********************************************/

template < class IngredientsType>
class ReadDebyeHuckel: public ReadToDestination<IngredientsType>
{
public:
    ReadDebyeHuckel(IngredientsType& i):ReadToDestination<IngredientsType>(i){}
    virtual ~ReadDebyeHuckel(){}
    virtual void execute();
};

template <class IngredientsType>
class WriteDebyeHuckel:public AbstractWrite<IngredientsType>
{
public:
    WriteDebyeHuckel(const IngredientsType& i)
        :AbstractWrite<IngredientsType>(i){this->setHeaderOnly(true);}

    virtual ~WriteDebyeHuckel(){}
    virtual void writeStream(std::ostream& strm);
};

template < class IngredientsType>
class ReadDebyeHuckelCutoff: public ReadToDestination<IngredientsType>
{
public:
    ReadDebyeHuckelCutoff(IngredientsType& i):ReadToDestination<IngredientsType>(i){}
    virtual ~ReadDebyeHuckelCutoff(){}
    virtual void execute();
};

template <class IngredientsType>
class WriteDebyeHuckelCutoff:public AbstractWrite<IngredientsType>
{
public:
    WriteDebyeHuckelCutoff(const IngredientsType& i)
        :AbstractWrite<IngredientsType>(i){this->setHeaderOnly(true);}

    virtual ~WriteDebyeHuckelCutoff(){}
    virtual void writeStream(std::ostream& strm);
};
class FeatureDebyeHuckel:public Feature
{

public:
	typedef LOKI_TYPELIST_1(FeatureBoltzmann) required_features_back;
	typedef LOKI_TYPELIST_2(FeatureBox,FeatureChargedMonomers) required_features_front;

	FeatureDebyeHuckel()
	:bjerrumLength(0.0),kappa(0.0),cutoffRadius(0.0)
	,verletSkin(0.0),useVerletList(false),verletListIsOutdated(true),shiftPotential(true){};
	
	
	virtual ~FeatureDebyeHuckel(){/*if(energyLookup!=0) delete [] energyLookup;*/}
	
	//configureing the potential ///////////////////////////////////////////
	void setBjerrumLength(double bl) {bjerrumLength=bl;}
	void setKappa(double k){kappa=k;}
	
	double getBjerrumLength() const {return bjerrumLength;}
	double getKappa() const {return kappa;}
	
	double getCutoffRadius() const {return cutoffRadius;}
	void setCutoffRadius(double r){cutoffRadius=r;}
	
	void setShiftPotentialToZero(bool val){shiftPotential=val;}
	
	// configure the verlet list ///////////////////////////////////////////
	void setUseVerletList(bool val){useVerletList=val;}
	
	double getVerletSkin() const {return verletSkin;} 
	void setVerletSkin(double s){verletSkin=s;}
	
	size_t getVerletListLength() const {return verletList.size();}
	
	template<class IngredientsType>
	double getCurrentDebyePotential(const IngredientsType& ingredients) const;
	
	//check- and apply-functions for different types of moves /////////////
	template<class IngredientsType>
	bool checkMove(const IngredientsType& ingredients,const MoveBase& move) const;
	
	template<class IngredientsType,class MoveType>
	bool checkMove(const IngredientsType& ingredients, MoveLocalBase<MoveType>& move);
	
	template<class IngredientsType>
	void applyMove(IngredientsType& ing, const MoveBase& move){}
	
	template<class IngredientsType,class MoveType>
	void applyMove(IngredientsType& ingredients,const MoveLocalBase<MoveType>& move);
	
	template<class IngredientsType>
	void synchronize(IngredientsType& ingredients);
	
	//export read and write functionality //////////////////////////////////
	template <class IngredientsType>
	void exportRead(FileImport <IngredientsType>& fileReader);
	
	template <class IngredientsType>
	void exportWrite(AnalyzerWriteBfmFile <IngredientsType>& fileWriter) const;
	
	void printMetaData(std::ostream& stream) const 
  	{
		stream << "\tbjerrum length: " << getBjerrumLength() << std::endl;
		stream << "\tdebye length: " << 1.0/getKappa() << std::endl;
		stream << "\tcutoff length: " << getCutoffRadius() << std::endl;
		stream << "\tused verlet lists: "<<useVerletList<<std::endl;
	}
private:
	
	// stuff for potential /////////////////////////////////////////////////
	inline double potential(const int squareDistance, const int charge1, const int charge2) const;
	double kappa,bjerrumLength;
	//using an array may be faster by a little bit, but it did not make 
	//any difference on my machine..vector brings easier usage
	std::vector<double> energyLookup;
	
	// stuff for verlet list ///////////////////////////////////////////////
	template<class IngredientsType>
	void updateVerletList(const IngredientsType& ingredients);
	
	bool useVerletList;
	bool verletListIsOutdated;
	double verletSkin;
	double cutoffRadius;
	bool shiftPotential;
	
	std::vector<size_t> verletList;
	std::map<size_t,size_t> verletNeigborsMap;
	std::map<size_t,size_t> verletIndexMap;
	std::map<size_t,VectorInt3> movedDistanceSinceListUpdate;
	
	
};


///////////////////////////////////////////////////////////////////////////////


template<class IngredientsType>
double FeatureDebyeHuckel::getCurrentDebyePotential(const IngredientsType& ingredients) const
{
	
	const std::vector<size_t>& chargedMonomers=ingredients.getChargedMonomers();
	double energy=0.0;
	for(size_t n=0;n<chargedMonomers.size();n++)
	{
		size_t monoIdxA=chargedMonomers[n];
		int16_t monoChargeA=ingredients.getMolecules()[monoIdxA].getCharge();
		VectorInt3 posA=ingredients.getMolecules()[monoIdxA];
		
		for(size_t m=0;m<n;m++)
		{
			size_t monoIdxB=chargedMonomers[m];
			int16_t monoChargeB=ingredients.getMolecules()[monoIdxB].getCharge();	
			VectorInt3 posB=ingredients.getMolecules()[monoIdxB];
			
			VectorInt3 distVector=Lemonade::calcDistanceVector3D(posA,posB,ingredients);
			energy+=potential(distVector*distVector,monoChargeA,monoChargeB);
			
		}
	}
	return energy;

}



template<class IngredientsType>
bool FeatureDebyeHuckel::checkMove(const IngredientsType& ingredients, const MoveBase& move) const
{
	return true;
}


template<class IngredientsType,class MoveType>
bool FeatureDebyeHuckel::checkMove(const IngredientsType& ingredients, MoveLocalBase<MoveType>& move)
{
	
	if(ingredients.getMolecules()[move.getIndex()].getCharge()==0) return true;
	else if (useVerletList==false)
	{
		double oldEnergy=0.0;
		double newEnergy=0.0;
		VectorInt3 myPos=ingredients.getMolecules()[move.getIndex()];
		VectorInt3 myPosPlusDelta=ingredients.getMolecules()[move.getIndex()]+move.getDir();
		size_t myIdx=move.getIndex();
		int16_t myCharge=ingredients.getMolecules()[myIdx].getCharge();

		const std::vector<size_t>& chargedMonomers=ingredients.getChargedMonomers();
		
		for(size_t n=0;n<chargedMonomers.size();n++)
		{
			size_t monoIdx=chargedMonomers[n];
			int16_t monoCharge=ingredients.getMolecules()[monoIdx].getCharge();
			
			if(monoIdx!=myIdx)
			{
				
				VectorInt3 pos=ingredients.getMolecules()[monoIdx];
				VectorInt3 distVectorOld=Lemonade::calcDistanceVector3D(myPos,pos,ingredients);
				VectorInt3 distVectorNew=Lemonade::calcDistanceVector3D(myPosPlusDelta,pos,ingredients);
				
				
				
				/*is this correct? in priciple, this should be true for like charges, and
				 the opposite for opposite charges. but the potential diverges for r=0. not sure what is 
				 the correct thing to do. this seems save. normally everything is ok 
				 if excl. volume is used*/
				if(myPosPlusDelta==pos) return false;
					
				else
				{
					oldEnergy+=potential(distVectorOld*distVectorOld,myCharge,monoCharge);
					newEnergy+=potential(distVectorNew*distVectorNew,myCharge,monoCharge);
				}
			}
		}
		
		move.multiplyProbability( std::exp(oldEnergy-newEnergy));
		return true;
	}
	else //if useVerletList==true
	{
		if(verletListIsOutdated==true) 
		{
			updateVerletList(ingredients);
			verletListIsOutdated=false;
		}
		
		double oldEnergy=0.0;
		double newEnergy=0.0;
		VectorInt3 myPos=ingredients.getMolecules()[move.getIndex()];
		VectorInt3 myPosPlusDelta=ingredients.getMolecules()[move.getIndex()]+move.getDir();
		size_t myIdx=move.getIndex();
		int16_t myCharge=ingredients.getMolecules()[myIdx].getCharge();

		
		size_t numberOfNeighbors=verletNeigborsMap[myIdx];
		size_t verletListIndex=verletIndexMap[myIdx];
		size_t neighborIndex;
		int16_t neighborCharge;
		
		for(size_t n=0;n<numberOfNeighbors;n++)
		{
			neighborIndex=verletList[verletListIndex+n];
			neighborCharge=ingredients.getMolecules()[neighborIndex].getCharge();
			
			VectorInt3 pos=ingredients.getMolecules()[neighborIndex];
			VectorInt3 distVectorOld=Lemonade::calcDistanceVector3D(myPos,pos,ingredients);
			VectorInt3 distVectorNew=Lemonade::calcDistanceVector3D(myPosPlusDelta,pos,ingredients);
			
			/*is this correct? in priciple, this should be true for like charges, and
			 t he opposite for opposite charges. b*ut the potential diverges for r=0. not sure what is 
			 the correct thing to do. this seems save. normally everything is ok 
			 if excl. volume is used*/
			if(myPosPlusDelta==pos) return false;			
			else
			{
				oldEnergy+=potential(distVectorOld*distVectorOld,myCharge,neighborCharge);
				newEnergy+=potential(distVectorNew*distVectorNew,myCharge,neighborCharge);
			}
		}
	
		
		move.multiplyProbability( std::exp(oldEnergy-newEnergy));
		return true;
	}
}



template<class IngredientsType,class MoveType>
void FeatureDebyeHuckel::applyMove(IngredientsType& ing, const MoveLocalBase<MoveType>& move)
{
	if(ing.getMolecules()[move.getIndex()].getCharge()!=0 && useVerletList==true)
	{	         
		movedDistanceSinceListUpdate[move.getIndex()]+=move.getDir();
		
		if(double(movedDistanceSinceListUpdate[move.getIndex()]*movedDistanceSinceListUpdate[move.getIndex()]) >= verletSkin*verletSkin/4.0)
		{
			verletListIsOutdated=true;
		}
	}
}

template<class IngredientsType>
void FeatureDebyeHuckel::synchronize(IngredientsType& ingredients)
{
	//setup the energy lookup containing the factors bjerrumLength*exp(-r/debyeLength)/r
	/*
	if(energyLookup!=0) delete[] energyLookup;
	energyLookup=0;
	*/
	
	int maxX=ingredients.getBoxX()/2;
	int maxY=ingredients.getBoxY()/2;
	int maxZ=ingredients.getBoxZ()/2;
	int maxRsquared=maxX*maxX+maxY*maxY+maxZ*maxZ;
	
	if(cutoffRadius==0.0)
		cutoffRadius=std::sqrt(double(maxRsquared));
	
#ifdef DEBUG	
	std::cout<<"FeatureDebyeHuckel::synchronize()...setting up potiential lookup of size "<<maxRsquared+1;
#endif //DEBUG
	energyLookup.resize(maxRsquared+1);
	energyLookup[0]=std::numeric_limits<double>::infinity();
	for(int n=1;n<maxRsquared+1;n++)
	{
		double r=std::sqrt(double(n));
		if(r<=cutoffRadius)
		{
			energyLookup[n]=bjerrumLength*std::exp(-r*kappa)/r;
			if(shiftPotential==true)
			{
				energyLookup[n]-=bjerrumLength*std::exp(-cutoffRadius*kappa)/cutoffRadius;
			}
		}
		else
			energyLookup[n]=0.0;
	}
#ifdef DEBUG	
	std::cout<<"...done\n";	
	std::cout<<"FeatureDebyeHuckel::synchronize()...using cutoff radius "<<cutoffRadius<<"\n"; 
#endif //DEBUG
	
	if(useVerletList==true)
	{
		std::cout<<"FeatureDebyeHuckel::synchronize()...start setting up Verlet list...\n"; 
		updateVerletList(ingredients);
		verletListIsOutdated=false;
		std::cout<<"FeatureDebyeHuckel::synchronize()...done setting up Verlet list...\n"; 
	}
	else
	{
		verletIndexMap.clear();
		verletNeigborsMap.clear();
		movedDistanceSinceListUpdate.clear();
		verletList.clear();
	}
	
}


template<class IngredientsType>
void FeatureDebyeHuckel::exportRead(FileImport< IngredientsType >& fileReader)
{
	fileReader.registerRead("#!debye_huckel_potential",new ReadDebyeHuckel<FeatureDebyeHuckel>(*this));
	fileReader.registerRead("#!debye_huckel_cutoff",new ReadDebyeHuckelCutoff<FeatureDebyeHuckel>(*this));
}

template<class IngredientsType>
void FeatureDebyeHuckel::exportWrite(AnalyzerWriteBfmFile <IngredientsType>& fileWriter) const
{
	fileWriter.registerWrite("#!debye_huckel_potential",new WriteDebyeHuckel<FeatureDebyeHuckel>(*this));
	fileWriter.registerWrite("#!debye_huckel_cutoff",new WriteDebyeHuckelCutoff<FeatureDebyeHuckel>(*this));
}


inline double FeatureDebyeHuckel::potential(const int squareDistance,const int charge1, const int charge2) const
{
	double energy=double(charge1*charge2)*energyLookup[squareDistance];
	return energy;
}

template<class IngredientsType>
void FeatureDebyeHuckel::updateVerletList(const IngredientsType& ingredients)
{
	//reset all list related containers
	verletIndexMap.clear();
	verletNeigborsMap.clear();
	movedDistanceSinceListUpdate.clear();
	verletList.clear();
	
	const std::vector<size_t>& chargedMonomers=ingredients.getChargedMonomers();
	
	VectorInt3 distanceVector, posN,posM;
	
	double verletListRadiusSquared=(cutoffRadius+verletSkin)*(cutoffRadius+verletSkin);
	
	
	size_t neighborCount=0;
	
	for(size_t n=0;n<chargedMonomers.size();n++)
	{
		posN=ingredients.getMolecules()[chargedMonomers[n]];
		
		for(size_t m=0;m<chargedMonomers.size();m++)
		{
			if(m!=n)
			{
				
				posM=ingredients.getMolecules()[chargedMonomers[m]];
				distanceVector=Lemonade::calcDistanceVector3D(posN,posM,ingredients);
				if(distanceVector*distanceVector <= verletListRadiusSquared)
				{
					verletList.push_back(chargedMonomers[m]);
					neighborCount++;
				}
			}
		}
		
		verletNeigborsMap.insert(std::make_pair(chargedMonomers[n],neighborCount));
		
		if(neighborCount>0)
		{
			//verletIndexMap.insert(make_pair(chargedMonomers[n],tmpVerletList.size()-neighborCount));
			verletIndexMap.insert(std::make_pair(chargedMonomers[n],verletList.size()-neighborCount));
		}	
		
		neighborCount=0;
		
		movedDistanceSinceListUpdate.insert(std::make_pair(chargedMonomers[n],VectorInt3(0,0,0)));
	}
	
}



///////////////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void ReadDebyeHuckel<IngredientsType>::execute()
{
    IngredientsType& ingredients=this->getDestination();
    std::istream& file=this->getInputStream();

    double bjerrumLength,kappa;
    
    file>>bjerrumLength;

    if(file.fail())
    {
        std::stringstream messagestream;
        messagestream<<"ReadDebyeHuckel::execute()\n"
                    <<"Could not read bjerrumLength (first parameter)\n";
        throw std::runtime_error(messagestream.str());
    }

    //now read salt concentration
    file>>kappa;

    if(file.fail())
    {
        std::stringstream messagestream;
        messagestream<<"ReadDebyeHuckel::execute()\n"
                    <<"Could not read kappa (second parameter)\n";
        throw std::runtime_error(messagestream.str());
    }


    //now save the interaction tuple just read from the file
    ingredients.setBjerrumLength(bjerrumLength);
    ingredients.setKappa(kappa);

}


template<class IngredientsType>
void WriteDebyeHuckel<IngredientsType>::writeStream(std::ostream& stream)
{
    
    stream<<"## format: #!debye_huckel_potential bjerrumLength kappa(=1/debye-length)\n";
    stream<<"#!debye_huckel_potential "<<this->getSource().getBjerrumLength()<<" "<<this->getSource().getKappa()<<"\n";
    stream<<"\n\n";

}

///////////////////////////////////////////////////////////////////////
template<class IngredientsType>
void ReadDebyeHuckelCutoff<IngredientsType>::execute()
{
    IngredientsType& ingredients=this->getDestination();
    std::istream& file=this->getInputStream();

    double cutoff;
    
    file>>cutoff;

    if(file.fail())
    {
        std::stringstream messagestream;
        messagestream<<"ReadDebyeHuckelCutoff::execute()\n"
                    <<"Could not read potential cutoff radius\n";
        throw std::runtime_error(messagestream.str());
    }

    ingredients.setCutoffRadius(cutoff);

}


template<class IngredientsType>
void WriteDebyeHuckelCutoff<IngredientsType>::writeStream(std::ostream& stream)
{
    
    stream<<"#!debye_huckel_cutoff "<<this->getSource().getCutoffRadius()<<"\n";
    stream<<std::endl;

}
#endif // FEATURE_DEBYEHUCKEL_H
