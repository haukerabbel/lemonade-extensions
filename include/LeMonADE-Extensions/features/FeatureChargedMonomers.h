#ifndef FEATURE_CHARGEDMONOMERS_H
#define FEATURE_CHARGEDMONOMERS_H

#include <LeMonADE/io/FileImport.h>
#include <LeMonADE/analyzer/AnalyzerWriteBfmFile.h>
#include <LeMonADE/feature/Feature.h>
#include <LeMonADE/io/AbstractRead.h>
#include <LeMonADE/io/AbstractWrite.h>
#include <LeMonADE/updater/moves/MoveBase.h>
#include <LeMonADE/updater/moves/MoveLocalSc.h>


/**
 * @file
 * @brief All classes used by FeatureChargedMonomers (FeatureChargedMonomers,MonomerMovableTag,ReadChargedMonomers,WriteChargedMonomers)
 * */


/******************************************************************************
 * class definitions
 * ****************************************************************************/

/**
 * @class ChargedMonomer
 * @brief Extends monomers by an 16bit integer charge along with getter and setter.
 *
 * @details Initially the charge is set to 0.
 **/
class ChargedMonomer
{
public:
  //! constructor sets initial tag to true==moveable
  ChargedMonomer():charge(0){}

  /**
   * @brief Getting charge of the monomer.
   *
   * @return Integer charge of monomer
   */
  int16_t getCharge() const {return charge;}

  /**
   * @brief Setting the charge of the monomer.
   *
   * @param _charge Integer charge of monomer.
   */
  void setCharge(int16_t _charge){ charge=_charge;}

private:

  //! Private charge of monomer
  int16_t charge;
};


/**
 * @class ReadChargedMonomers
 *
 * @brief Handles BFM-File-Reads \b #!charged_monomers.
 *
 * @tparam IngredientsType Ingredients class storing all system information.
 **/
template < class IngredientsType> 
class ReadChargedMonomers: public ReadToDestination<IngredientsType>
{
public:
  ReadChargedMonomers(IngredientsType& i):ReadToDestination<IngredientsType>(i){}
  virtual ~ReadChargedMonomers(){}
  virtual void execute();
};

/**
 * @class WriteChargedMonomers
 *
 * @brief Handles BFM-File-Write \b #!charged_monomers
 *
 * @tparam IngredientsType Ingredients class storing all system information.
 **/
template <class IngredientsType> 
class WriteChargedMonomers:public AbstractWrite<IngredientsType>
{
public:
	//! Only writes \b #!fixed_monomers into the header of the bfm-file.
  WriteChargedMonomers(const IngredientsType& i)
    :AbstractWrite<IngredientsType>(i){this->setHeaderOnly(true);}
  virtual ~WriteChargedMonomers(){}
  virtual void writeStream(std::ostream& strm);
};

/**
 * @class FeatureChargedMonomers
 *
 * @brief Extends vertex/monomer by an integer charge and provides read/write functionality.
 **/
class FeatureChargedMonomers:public Feature
{
public:
	typedef LOKI_TYPELIST_1(ChargedMonomer) monomer_extensions;
	
	FeatureChargedMonomers():totalCharge(0){};
	const std::vector<size_t>& getChargedMonomers() const {return chargedMonomers;}
	
	
	//! Export the relevant functionality for reading bfm-files to the responsible reader object
	template<class IngredientsType> 
	void exportRead(FileImport<IngredientsType>& fileReader);
	
	//! Export the relevant functionality for writing bfm-files to the responsible writer object
	template<class IngredientsType> 
	void exportWrite(AnalyzerWriteBfmFile<IngredientsType>& fileWriter) const;

	/**
	* @brief Check move for all unknown moves: this does nothing
	*
	* @details Returns true for all moves other than the ones that have specialized versions of this function.
	* This dummy function is implemented for generality.
	*
	* @param [in] ingredients A reference to the IngredientsType - mainly the system
	* @param [in] move General move other than MoveLocalBase (MoveLocalSc or MoveLocalBcc).
	* @return true Always!
	*/
	template<class IngredientsType>
	bool checkMove(const IngredientsType& ingredients, const MoveBase& move) const
	{
		return true;
	}
  
  	void printMetaData(std::ostream& stream) const 
  	{
		stream << "\tnumber of charged monomers: " << chargedMonomers.size() << std::endl;
		stream << "\ttotal charge of system " << totalCharge << std::endl;
	}
	
	template<class IngredientsType>
	void synchronize(IngredientsType& ingredients)
	{
		chargedMonomers.clear();
		totalCharge=0;
		for(size_t n=0;n<ingredients.getMolecules().size();n++)
		{
			totalCharge+=ingredients.getMolecules()[n].getCharge();
			
			if(ingredients.getMolecules()[n].getCharge()!=0)
			{
				chargedMonomers.push_back(n);
			}
		}
	}
	
private:
	std::vector<size_t> chargedMonomers;
	int32_t totalCharge;

};


/******************************************************************************
 * member implementations
 * ****************************************************************************/

/**
 * @details The function is called by the Ingredients class when an object of type Ingredients
 * is associated with an object of type FileImport. The export of the Reads is thus
 * taken care automatically when it becomes necessary.\n
 * Registered Read-In Commands:
 * * #!charged_monomers
 *
 * @param fileReader File importer for the bfm-file
 * @tparam IngredientsType Features used in the system. See Ingredients.
 **/
template<class IngredientsType> 
void FeatureChargedMonomers::exportRead(FileImport< IngredientsType >& fileReader)
{
  fileReader.registerRead("#!charged_monomers",new ReadChargedMonomers<IngredientsType>(fileReader.getDestination()));
}


/******************************************************************************/
/**
 * The function is called by the Ingredients class when an object of type Ingredients
 * is associated with an object of type AnalyzerWriteBfmFile. The export of the Writes is thus
 * taken care automatically when it becomes necessary.\n
 * Registered Write-Out Commands:
 * * #!charged_monomers
 *
 * @param fileWriter File writer for the bfm-file.
 */
template<class IngredientsType> 
void FeatureChargedMonomers::exportWrite(AnalyzerWriteBfmFile< IngredientsType >& fileWriter) const
{
  fileWriter.registerWrite("#!charged_monomers",new WriteChargedMonomers<IngredientsType>(fileWriter.getIngredients_()));
}

/******************************************************************************/
/**
 * @brief Executes the reading routine to extract \b #!charged_monomers.
 *
 * @throw <std::runtime_error> attributes and identifier could not be read.
 **/
template < class IngredientsType> 
void ReadChargedMonomers<IngredientsType>::execute()
{
	std::cout << " executeReadChargedMonomers"<< std::endl;

	//some variables used during reading
	//counts the number of fixedMonomers lines in the file
	int nChargedMonomers=0;
	int startIndex,stopIndex;
	int16_t MonomerCharge;
	//contains the latest line read from file
	std::string line;
	//used to reset the position of the get pointer after processing the command
	std::streampos previous;
	//for convenience: get the input stream
	std::istream& source=this->getInputStream();
	//for convenience: get the set of monomers
	typename IngredientsType::molecules_type& molecules=this->getDestination().modifyMolecules();
	
	//go to next line and save the position of the get pointer into streampos previous
	getline(source,line);
	previous=(source).tellg();

	//read and process the lines containing the charge definition
	getline(source,line);
	
	while(!line.empty() && !((source).fail()))
	{
	
		//stop at next Read and set the get-pointer to the position before the Read
		if(this->detectRead(line))
		{
			(source).seekg(previous);
			break;
		}

		//initialize stringstream with content for ease of processing
		std::stringstream stream(line);

		//starting index for this block
		stream>>startIndex;

		//throw exception, if extraction fails
		if(stream.fail())
		{
			std::stringstream messagestream;
			messagestream<<"ReadChargedMonomers<IngredientsType>::execute()\n"
					<<"Could not read first index in charged_monomers line "<<nChargedMonomers+1;
			throw std::runtime_error(messagestream.str());
		}
		
		//throw exception, if next character isnt "-"
		if(!this->findSeparator(stream,'-'))
		{
		
			std::stringstream messagestream;
			messagestream<<"ReadChargedMonomers<IngredientsType>::execute()\n"
					<<"Wrong definition of charged_monomers\nCould not find separator \"-\" "
					<<"in charged_monomers definition no "<<nChargedMonomers+1;
			throw std::runtime_error(messagestream.str());
		
		}

		//read second index of this block, throw exception if extraction fails
		stream>>stopIndex;

		//throw exception, if extraction fails
		if(stream.fail())
		{
			std::stringstream messagestream;
			messagestream<<"ReadChargedMonomers<IngredientsType>::execute()\n"
					<<"Could not read second index in charged_monomers line "<<nChargedMonomers+1;
			throw std::runtime_error(messagestream.str());
		}
		
		//throw exception, if next character isnt ":"
		if(!this->findSeparator(stream,':'))
		{
		
			std::stringstream messagestream;
			messagestream<<"ReadChargedMonomers<IngredientsType>::execute()\n"
					<<"Wrong definition of charged_monomers\nCould not find separator \":\" "
					<<"in fixed_monomers definition no "<<nChargedMonomers+1;
			throw std::runtime_error(messagestream.str());
		
		}
		//read the MonomerCharge
		stream>>MonomerCharge;
		//if extraction worked, save the attributes
		if(!stream.fail())
		{

			//save attributes
			for(int n=startIndex;n<=stopIndex;n++)
			{
					//use n-1 as index, because bfm-files start counting indices at 1 (not 0)
					molecules[n-1].setCharge(MonomerCharge);

			}
			nChargedMonomers++;
			getline((source),line);

		}
		//otherwise throw an exception
		else
		{
		
			std::stringstream messagestream;
			messagestream<<"ReadChargedMonomers<IngredientsType>::execute()\n"
					<<"could not read monomer charge in charged_monomers definition no "<<nChargedMonomers+1;
			throw std::runtime_error(messagestream.str());

		}
	} 
}

/******************************************************************************/
//! Executes the routine to write \b #!charged_monomers.
template < class IngredientsType>
void WriteChargedMonomers<IngredientsType>::writeStream(std::ostream& strm)
{
  //for all output the indices are increased by one, because the file-format
  //starts counting indices at 1 (not 0)
  
  //write bfm command
  strm<<"## charged_monomers: default charge is 0\n";
  strm<<"#!charged_monomers\n";
  //get reference to monomers
  const typename IngredientsType::molecules_type& molecules=this->getSource().getMolecules();
  
  size_t nMonomers=molecules.size();
  //attribute blocks begin with startIndex
  size_t startIndex=0;
  //counter varable
  size_t n=0;
  //attribute to be written (updated in loop below)
  int8_t MonomerCharge=molecules[0].getCharge(); 	  
  
  //write attibutes (blockwise)
  while(n<nMonomers){
    
    if(molecules[n].getCharge()!=MonomerCharge)
    {
      if(MonomerCharge!=0) strm<<startIndex+1<<"-"<<n<<":"<<int(MonomerCharge)<<std::endl;
      MonomerCharge=molecules[n].getCharge();
      startIndex=n;
    }
    n++;
  }
  //write final charged monomers
  if(MonomerCharge!=0)
	  strm<<startIndex+1<<"-"<<nMonomers<<":"<<int(MonomerCharge);
  
  strm<<std::endl<<std::endl;
  
}


#endif /*FEATURE_CHARGEDMONOMERS_H*/
