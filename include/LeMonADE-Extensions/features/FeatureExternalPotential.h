#ifndef FEATURE_EXTERNAL_POTENTIAL_H
#define FEATURE_EXTERNAL_POTENTIAL_H

#include <LeMonADE/updater/moves/MoveBase.h>
#include <LeMonADE/updater/moves/MoveLocalSc.h>
#include <LeMonADE/utility/DistanceCalculation.h>
#include <LeMonADE/feature/Feature.h>
#include <LeMonADE/feature/FeatureBoltzmann.h>
#include <LeMonADE/feature/FeatureAttributes.h>

template<class PotentialType>
class FeatureExternalPotential:public Feature{
public:

    typedef LOKI_TYPELIST_1(FeatureBoltzmann) required_features_back;
    typedef LOKI_TYPELIST_1(FeatureAttributes) required_features_front;
	
    FeatureExternalPotential(){}
    virtual ~FeatureExternalPotential(){}
	
    const PotentialType& getPotential() const {return potential;}
    PotentialType& modifyPotential() {return potential;}
	
//for all unknown moves: does nothing
    template < class IngredientsType> 
    bool checkMove( const IngredientsType& ingredients, const MoveBase& move ) const;
//overload for MoveLocalSc
    template < class IngredientsType> 
    bool checkMove( const IngredientsType& ingredients, MoveLocalSc& move );

//for unknown moves(does nothing)
    template<class IngredientsType> 
    void applyMove(IngredientsType& ing, const MoveBase& move){}
	
//for moves of type MoveLocalSc
    template<class IngredientsType> 
    void applyMove(IngredientsType& ing, const MoveLocalSc& move);
    
    template<class IngredientsType>
    void synchronize(IngredientsType& ingredients);
    
private:
	
    PotentialType potential;
	
};



///////////////////////////////////////////////////////////////////////////////
////////////////////Implementation of methods////////////////////////////


template<class PotentialType>
template<class IngredientsType>
bool FeatureExternalPotential<PotentialType>::checkMove(const IngredientsType& ingredients, 
							const MoveBase& move) const
{
    return true;//nothing to do for unknown moves
}


template<class PotentialType>
template<class IngredientsType>
bool FeatureExternalPotential<PotentialType>::checkMove(const IngredientsType& ingredients, 
							MoveLocalSc& move)
{
    int32_t attribute=ingredients.getMolecules()[move.getIndex()].getAttributeTag();
    VectorInt3 position=ingredients.getMolecules()[move.getIndex()];
	
    //calculate the potential at the hypothetical new position
    double currentPotential=potential(position,attribute);
    double newPotential=potential(position+move.getDir(),attribute);
	
    //calculate change in potential energy
    double potentialGainAfterMove=newPotential-currentPotential;
	
    //add probability to the move, if the new energy is not infinity
    if(potentialGainAfterMove==std::numeric_limits<double>::infinity())
	return false;
    else if (potentialGainAfterMove==0.0)
	return true;
    else
    {
	move.multiplyProbability(std::exp(-potentialGainAfterMove));
	return true;
    }
		
}


//if the move is applied, the bookkeeping variables previousPotential 
//and previousMonomerGroupCOM_Z are updated
template<class PotentialType>
template<class IngredientsType>
void FeatureExternalPotential<PotentialType>::applyMove(IngredientsType& ingredients, const MoveLocalSc& move)
{
    	
}


//finds the indices of the affected monomers and checks if the
//center of mass is in a valid position, i.e. that the potenial is not
//infinite
template<class PotentialType>
template<class IngredientsType>
void FeatureExternalPotential<PotentialType>::synchronize(IngredientsType& ingredients)
{
    potential.setup(ingredients);
}




#endif //FEATURE_EXTERNAL_POTENTIAL_H
