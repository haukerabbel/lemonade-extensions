#ifndef TIME_SERIES_H
#define TIME_SERIES_H

#include<iostream>

template<class DataType> class TimeSeries
{
public:
  TimeSeries(){}
  
  /*****************************************************************************/
  /**
   * @brief Combine two time series into on with averaged data (different lengths are allowed)
   **/
  TimeSeries<DataType>& operator+=(TimeSeries<DataType>& rhs);
  
  /*****************************************************************************/
  /**
   * @brief Add a new data point to the time series. 
   **/
  void add(std::pair<uint64_t,DataType> data);
  void add(uint64_t time, DataType data);
  
  /*****************************************************************************/
  /**
   * @brief Returns the complete, averaged time series as a map of time to value
   **/
  std::map<uint64_t,DataType> get();
  
  /*****************************************************************************/
  /**
   * @brief Returns a name string, e.g. for putting together filenames
   **/
  static std::string getName() { return "TimeSeries" ;}
  
  /*****************************************************************************/
  /**
   * @brief writes formatted result into file stream
   **/
  void write(std::ofstream& out);

  /*****************************************************************************/
  /**
   * @brief clears the data
   **/
  void clear(){data.clear();nValues.clear();}

  /*****************************************************************************/
  /**
   * @brief clears the data
   **/
  size_t size(){return data.size();}
  
  
private:
  /*****************************************************************************/
  /**
   * @var data 
   * @brief Map of timestamp to accumulated data
   * 
   * @var nValues
   * @brief Map of timestamp to number of data points. Final result is data[i]/nValues[i]
   **/
  std::map<uint64_t,DataType>  data;
  std::map<uint64_t,uint64_t>  nValues;
};

/*****************************************************************************/
/******************************************************************************
 * implementation of class TimeSeries
 * ****************************************************************************/

/*****************************************************************************/
//operator+=
/*****************************************************************************/
template<class DataType>
TimeSeries<DataType >& TimeSeries<DataType>::operator+=(TimeSeries< DataType >& rhs)
{
  
    typedef typename std::map<uint64_t,DataType> ::iterator iterator;
    //add data and nValues from rhs. If a timestamp exists in rhs, but not here,
    //it is created due to behaviour of operator[] of std::map. thus, all data survive.
    for(iterator it=rhs.data.begin();it!=rhs.data.end();++it){
      data[it->first]+=it->second;
      nValues[it->first]+=rhs.nValues[it->first];
    }
    //return reference to self
    return *this;
}

/*****************************************************************************/
//add(...). Adding of new values is only allowed for increasing timesteps
/*****************************************************************************/
template<class DataType>
void TimeSeries<DataType>::add(std::pair<uint64_t,DataType> dataPoint)
{
  //add new value to the end of map, if timestep is increasing. throw exception otherwise.
  if(data.size()!=0 && dataPoint.first > data.end()->first){
    data.insert(data.end(),dataPoint);
    nValues.insert(nValues.end(),std::pair<uint64_t,uint64_t>(dataPoint.first,1));
  }
  //if no values there yet, add the new data in any case
  else if(data.size()==0){
    data.insert(data.end(),dataPoint);
    nValues.insert(std::pair<uint64_t,uint64_t>(dataPoint.first,1));
  }
  //throw exception of timestep is not growing
  else
    throw std::runtime_error("TimeSeries::add  Time series is not continuous.");
}

template<class DataType>
void TimeSeries<DataType>::add(uint64_t time, DataType data)
{
	add(std::make_pair(time,data));
}

/*****************************************************************************/
//get()
/*****************************************************************************/
template<class DataType>
std::map<uint64_t, DataType > TimeSeries<DataType>::get()
{
    //calculate average data[i]/nValues[i], store series in map, and return it.
    std::map<uint64_t,DataType> tmp;
  
    typedef typename std::map <uint64_t,DataType>::iterator iterator;
    for (iterator it=data.begin();it!=data.end();++it){
      tmp.insert(std::pair<uint64_t,DataType>(it->first,(it->second)/nValues[it->first]));
    }
    
    return tmp;
}

/*****************************************************************************/
//write(...)
/*****************************************************************************/
template<class DataType>
void TimeSeries<DataType>::write(std::ofstream& out)
{
    //get correctly averaged result
    std::map<uint64_t,DataType> tmp=get();
    typedef typename std::map <uint64_t,DataType>::iterator iterator;
    
    //write complete series to file stream
    for (iterator it=tmp.begin();it!=tmp.end();++it){
      out<<it->first<<"\t"<<it->second<<std::endl;
    }
}

#endif