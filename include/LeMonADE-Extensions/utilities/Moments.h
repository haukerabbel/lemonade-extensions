#ifndef MOMENTS_H
#define MOMENTS_H

template <class ScalarType> class Moments
{
public:
  Moments():sum(0),squareSum(0),nValues(0){};
  
  //! Add a new value to the collection
  void add(ScalarType val){sum+=val;squareSum+=val*val;++nValues;}

  //! Reset collected values to 0
  void reset(){sum=0;squareSum=0;nValues=0;}

  //! Merge another object of the same type into this one, such that the samples of both are combined
  void merge(Moments<ScalarType>& rhs){sum+=rhs.sum;squareSum+=rhs.squareSum;nValues+=rhs.nValues;}
  
  //! Zeroth moment(number of samples)
  uint32_t m_0() const {return nValues;}

  //! First moment (average)
  double m_1() const{ return double(sum)/double(nValues);}

  //! Second moment (average square)
  double m_2() const{return double(squareSum)/double(nValues);}

  //! First central moment (always 0)
  double mu_1() const{return 0.0;}

  //! Second central moment (variance)
  double mu_2() const{return m_2()-m_1()*m_1();}
 
private:
  ScalarType sum;
  ScalarType squareSum;
  uint32_t nValues;
};

#endif 