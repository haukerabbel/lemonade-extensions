#ifndef LEMONADE_EXTENSIONS_ITERATOR_PREDICATES_H
#define LEMONADE_EXTENSIONS_ITERATOR_PREDICATES_H



class partOfUpperLeaflet
{
public:
	
	partOfUpperLeaflet(int type1,int type2,int32_t midplane){types.insert(type1); types.insert(type2);z0=midplane;}
	
	template<class MoleculesType>
	bool operator()(const MoleculesType& m, int i)
	{
		return (types.count(m[i].getAttributeTag()) ) && (m[i].getZ()-z0 >0);
	}
private:
	std::set<int> types;
	int32_t z0;
};

class partOfLowerLeaflet
{
public:
	partOfLowerLeaflet(int type1,int type2,int32_t midplane){types.insert(type1); types.insert(type2);z0=midplane;}
	
	template<class MoleculesType>
	bool operator()(const MoleculesType& m, int i)
	{
		return (types.count(m[i].getAttributeTag()) ) && (m[i].getZ()-z0 <0);
	}
private:
	std::set<int> types;
	int32_t z0;
};


class partOfUpperLeafletGPU
{
public:
	
	partOfUpperLeafletGPU(int type1,int type2,int32_t midplane){types.insert(type1);types.insert(type1+3); types.insert(type2);types.insert(type2+3);z0=midplane;}
	
	template<class MoleculesType>
	bool operator()(const MoleculesType& m, int i)
	{
		return (types.count(m[i].getAttributeTag()) ) && (m[i].getZ()-z0 >0);
	}
private:
	std::set<int> types;
	int32_t z0;
};

class partOfLowerLeafletGPU
{
public:
	partOfLowerLeafletGPU(int type1,int type2,int32_t midplane){types.insert(type1);types.insert(type1+3); types.insert(type2);types.insert(type2+3);z0=midplane;}
	
	template<class MoleculesType>
	bool operator()(const MoleculesType& m, int i)
	{
		return (types.count(m[i].getAttributeTag()) ) && (m[i].getZ()-z0 <0);
	}
private:
	std::set<int> types;
	int32_t z0;
};

#endif